<%@ page language="java" import="java.util.*" pageEncoding="utf-8" %>
<%@page import="com.lmpy.lmpy.core.bean.Member" %>
<%@page import="com.lmpy.lmpy.core.bean.Orders" %>
<%@page import="com.lmpy.lmpy.core.utils.CommonUtil" %>
<%@page import="com.lmpy.lmpy.core.bean.Teacher" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="renderer" content="webkit">
    <meta name="viewport" content="width=1200">


    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/css/css11.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/css/code1.css">
    <LINK rel="stylesheet" type="text/css" href="<%=basePath%>static/css/base1.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/css/brief1.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/css/pageSwitch.min1.css">

    <script type="text/javascript" src="<%=basePath%>static/js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>static/js/jquery-ui.js"></script>
    <script type="text/javascript" src="<%=basePath%>static/js/tool.js"></script>
    <script type="text/javascript" src="<%=basePath%>static/js/code.js"></script>

    <script src="<%=basePath%>static/js/jquery.artDialog.min.js" type="text/javascript"></script>
    <script src="<%=basePath%>static/js/jQuery.rTabs.js" type="text/javascript"></script>
    <script src="<%=basePath%>static/js/search_tab.js" type="text/javascript"></script>
    <script type="text/javascript" src="https://api.map.baidu.com/getscript?v=3.0&ak=wjgdegDpqWjj2Rabewsbe3Geeid37AP6"></script>
    <STYLE>
        * {
            padding: 0;
            margin: 0;
        }

        html,
        body {
            height: 100%;
        }

        #container {
            width: 100%;
            height: 500px;
            overflow: hidden;
        }

        .sections,
        .section {
            height: 100%;
        }

        #container,
        .sections {
            position: relative;
        }

        .section {
            background-color: #000;
            background-size: cover;
            background-position: 50% 50%;
            text-align: center;
            color: white;
        }


        .mainProNav {
            background: rgba(0, 0, 0, .7);
            width: 230px;
            height: 500px;
        }

        .mainProNav dt {
            color: #fff;
        }

        .mainProNav dt {
            padding-top: 15px;
            height: 80px;
            line-height: 18px;
            font-size: 16px;
        }

        .mainProNav .dlHover dt {
            padding-left: 24px;
            background: #000;
            line-height: 18px;
            padding-top: 15px;
            height: 80px;
        }

        .mainProNav p {
            height: 28px;
            padding-top: 10px;
            font-size: 12px;
            font-weight: 100;
            width: 180px;
        }

        .mainProNav dd {
            left: 230px;
            border-left: none
        }
    </STYLE>
</head>

<body>
<%
    Orders order = (Orders) request.getAttribute("order");
    if (order != null) {
%>


<div>
    <div class="fl  font_15" style="margin: 10px">
        <h4 class="th_line_d">学员信息</h4>
        <ul class="clearfix list_text_ul list_2">
            <li>订单编号：<%=order.getOrderCode() %>
            </li>
            <li>求教科目：<%=order.getCourseName() %>
            </li>
            <li>联系人：<%=order.getContactName().substring(0, 1) %>
                <%if (order.getContactGender() == 1) {%>
                先生
                <%} else { %>
                女士
                <%} %>
            </li>
            <li>联系电话：<%=order.getContactPhone() %>
            </li>
            <li>微信号：<%=order.getWxNumber() %>
            </li>

            <li>学员性别： <%if (order.getStudentGender() == 1) {%>
                男
                <%} else { %>
                女
                <%} %>
            </li>
            <li>薪酬标准：<%
                if (order.getCoursePrice() != 0) {
            %>
                <%=order.getCoursePrice() %>元/小时
                <%} else { %>
                参照云朵家教网标准
                <%} %>
            </li>
            <li>授课时间：
                <%
                    String[] teacherDays = order.getTeachingTime().split(",");
                    for (int j = 0; j < teacherDays.length; j++) {
                        String teacherDay = teacherDays[j];
                %>
                <%=CommonUtil.getTeacherTime(teacherDay)%>
                <%} %>
            </li>
            <li>年级：<%=order.getStudentGradeName() %>
            </li>
            <li>就读学校：</li>
            <li>学员状态：<%=order.getProfile() %>
            </li>
            <li></li>
        </ul>
        <h4 class="th_line_d">教员要求</h4>
        <ul class="clearfix list_text_ul list_2">
            <li>性别要求：<%if (order.getTeacherGender() == 0) {%>
                女
                <%} else if (order.getTeacherGender() == 1) { %>
                男
                <%} else if (order.getTeacherGender() == 2) { %>
                不限
                <%} %>
            </li>
            <li>身份要求：<%=order.getTeacherTypeName()%>
            </li>
            <li>需求人数：1 人</li>
            <li>上课方式：<%=order.getTeachingWayName() %>
            </li>
            <li class="li_w">其他要求：<%=order.getRequirements() %>
            </li>
        </ul>
        <div class="map_box shadow_box_light">
            <h5>居住位置：<%=order.getAddress() %>
            </h5>
            <div id="allmap" class="map-container"
                 style="overflow: hidden; position: relative; z-index: 0; background-color: rgb(243, 241, 236); color: rgb(0, 0, 0); text-align: left;">
                <div id="zoomer"
                     style="position: absolute; z-index: 0; top: 0px; left: 0px; overflow: hidden; visibility: hidden; cursor: url(https://api0.map.bdimg.com/images/openhand.cur) 8 8, default">
                    <div class="BMap_zoomer" style="top: 0; left: 0;"></div>
                    <div class="BMap_zoomer" style="top: 0; right: 0;"></div>
                    <div class="BMap_zoomer" style="bottom: 0; left: 0;"></div>
                    <div class="BMap_zoomer" style="bottom: 0; right: 0;"></div>
                </div>
            </div>
        </div>
        <!--end地图-->

    </div>
    <!--end一行的左-->
</div>
<%} %>
<!--end一行-->


<script>
    //百度地图API功能
    var map = new BMap.Map("allmap");            // 创建Map实例
    var address = encodeURI('<%=order.getAddress()%>');
    var myGeo = new BMap.Geocoder();
    myGeo.getPoint(address,
        function (point) {
            if (point) {
                map.centerAndZoom(point, 16);
                map.addOverlay(new BMap.Marker(point));
            }
        },
        "all");

    map.enableScrollWheelZoom();


</script>
</body>