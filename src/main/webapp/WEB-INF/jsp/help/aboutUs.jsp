<%@ page language="java" import="java.util.*" pageEncoding="utf-8" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<jsp:include page="header.jsp"></jsp:include>



<dl class="w_1200 site_th">
    <dd>
        当前位置：
        <a href="javascript:void(0)">首页</a><span class="china"></span>关于我们
    </dd>
</dl>

<div class="help_center font_15 w_1200 white_bj">
    <div class="fl help_l">

        <ul class="help_nav nav_ul">
            <li class="light">
                <a href="aboutUs">关于我们</a>
            </li>
            <li class="">
                <a href="iammember">我是学员</a>
            </li>
            <li class="">
                <a href="iamteacher">我是教员</a>
            </li>
            <li class="">
                <a href="disclaimer">免责说明</a>
            </li>
            <li class="">
                <a href="announcement">活动公告</a>
            </li>
            <li class="">
                <a href="link">友情链接</a>
            </li>
        </ul>
    </div>

    <!--end左边-->
    <div class="r_cell help_r">
        <h4 class="th_line_c">
            关于我们
        </h4>
        <p style="background: white; line-height: 26px; margin-bottom: 0px;">
			<span
                    style="color: rgb(85, 85, 85); font-family: 宋体; font-size: 14px;">&nbsp;
				&nbsp;</span>
        </p>
        <p >

            &nbsp;&nbsp;&nbsp;&nbsp;芦苇教育创立于2020年，是立足于互联网络，面向广大家教教员及学员的家教公司，致力于打造信息全面、功能优质的家教服务平台，提供便捷高效的家教服务。
        </p>
        <br>
        <p >
            &nbsp;&nbsp;&nbsp;&nbsp;作为教育产业的经营者，我们不断追求知识的学习和发展，从而使生存空间赢得文化内涵，并用深层的文化渗入规范我们的经营模式。在信息时代里，教育是个性化的教育，个性化教育是引导个体生命独特性发展的教育，它以尊重差异为前提，以提供多样化教育资源和自主选择为手段，以促进个体生命自由而充分的发展为目的。
        </p>
        <br>
        <p >
            &nbsp;&nbsp;&nbsp;&nbsp;芦苇教育的业务以郑州为起点，以线上教学、上门辅导等形式提供家教教学，为大学生、老师提供家教机会，为用户找到适合的家教老师，满足了广大用户对于家教、教育等相关信息的直接或潜在的需求。
        </p>
        <br>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;芦苇教育主要为有需要的用户提供一个方便、快捷、安全家教信息平台。在运营模式上，将传统的家教结合互联网技术，实行互动服务，全程跟踪家教服务，大大提高家教的透明度和家长的满意度。秉承“专注、专业”的工作作风，提供更优质的服务。您可以通过芦苇教育家教网按照自己需求选择合适的家教老师，并且能得到较大限度的优惠。您也可以寻找到适合自己的家教兼职、施展能力。
        </p>
        <br>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;对于学员：
        </p>
        <br>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;• 我们帮您寻找值得信赖的教员，让您享受到优质的服务&nbsp;
        </p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;• 我们提供海量的教员信息，能够第一时间联系看中的教员
        </p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;• 我们的教员信息不会有任何夸张虚假的成分，我们会保证教学水平和安全&nbsp;
        </p>
        <br>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;对于教员：
        </p>
        <br>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;• 我们为您寻找适合您的学员，让您学以致用&nbsp;
        </p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;• 我们提供便捷的方式，让您在短时间内敲定一份家教&nbsp;
        </p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;• 我们提供大量展示位，只要您付出就会得到更多的家教机会
        </p>
        <br>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;在取得成绩的同时我们不断寻求进步，未来我们将在各版改进，听取用户的建议和意见，推出更人性化更全面的功能。芦苇教育将一如既往地脚踏实地、锐意进取，为打造具有影响力的教育服务平台而努力。
        </p>
        <p
                style="background: white; line-height: 26px; text-indent: 25px; margin-bottom: 0px;">
            <br>
        </p>

    </div>
    <!--end右边-->
</div>
<!--end中间区域-->
<jsp:include page="../footer.jsp"></jsp:include>
