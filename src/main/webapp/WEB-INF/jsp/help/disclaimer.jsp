<%@ page language="java" import="java.util.*" pageEncoding="utf-8" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<jsp:include page="header.jsp"></jsp:include>


<dl class="w_1200 site_th">
    <dd>
        当前位置：
        <a href="javascript:void(0)">首页</a><span class="china">&gt;</span>我是学员
    </dd>
</dl>

<div class="help_center font_15 w_1200 white_bj">
    <div class="fl help_l">

        <ul class="help_nav nav_ul">
            <li class="">
                <a href="aboutUs">关于我们</a>
            </li>
            <li class="">
                <a href="iammember">我是学员</a>
            </li>
            <li class="">
                <a href="iamteacher">我是教员</a>
            </li>
            <li class="light">
                <a href="disclaimer">免责说明</a>
            </li>
            <li class="">
                <a href="announcement">活动公告</a>
            </li>
            <li class="">
                <a href="link">友情链接</a>
            </li>
        </ul>
    </div>

    <!--end左边-->
    <div class="r_cell help_r">
        <h4 class="th_line_c">
            免责声明
        </h4>
        <br/>
        <p style="background: white; line-height: 26px; margin-bottom: 0px;">
			<span
                    style="color: rgb(85, 85, 85); font-family: 宋体; font-size: 14px;">
            </span>
        </p >
        <p>
            如发生以下情况，吉师家教不对用户的直接或间接损失承担法律责任:
        </p>
        <br/>
        <p>
            一、网站更新维护
        </p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;1.1为了增进用户体验、完善服务内容，芦苇教育可能不断努力开发新的服务，并为您不时提供网站更新。
        </p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;1.2为了改善用户体验或提高服务安全性、保证功能的一致性等目的，芦苇教育有权不经向您特别通知而对本网站进行更新，或者对本网站的部分功能效果、服务内容进行改变。
        </p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;1.3如因系统维护或升级而需发暂停网络服务，芦苇教育将尽可能事先在网站进行遵知。
        </p>
        <br/>
        <p>
            二、用户个人信息
        </p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;2.1为增加用户使用本服务的舒适性，促进用户之间的沟通和交流，改善、优化相应服务，芦苇教育会对您的昵称、头像以及在本网站中的相关操作等信息进行使用。
        </p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;2.2芦苇教育将运用各种安全技术和程序建立完善的管理制度来保护您的个人信息，以免遭受未经授权的访问、使用或披露。
        </p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;2.3芦苇教育不会将您的个人信息转移或披露给任何第三方，除非：
        </p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;（1）相关法律法规或司法机关、行政机关要求；或
        </p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;（2）为完成合并、分立、收购或资产转让而转移；或
        </p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;（3）为提供您要求的服务所必需。
        </p>
        <br/>
        <p>
            三、用户注意事项
        </p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;3.1您理解并同意：为了向您提供有效的服务，本网站会利用您终端设备的处理器和带宽等资源。本网站使用过程中可能产生数据流量的费用，用户需自行向运营商了解相关资费信息，并自行承担相关费用。
        </p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;3.2您可以选择不向芦苇教育提供您的某些信息，或者根据产品设置取消芦苇教育收集某些信息的权利，但因此可能会导致相关服务功能无法实现。
        </p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;3.3芦苇教育将会尽其商业上的合理努力保障您在本服务中的数据存储安全，但是，芦苇教育并不能就此提供完全保证，包括但不限于以下情形：
        </p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;（1）如果您停止使用本网站和/或本服务，或因您违反法律法规或本协议而被取消或终止使用本服务，芦苇教育有权从服务器上永久地删除您的数据。您的服务停止、终止或取消后，芦苇教育没有义务向您返还任何数据。
        </p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;（2）芦苇教育对于那些经过延长期限仍未重新使用的替停帐户保留取消的权利，并可随时自主决定，发出或无须通知。
        </p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;3.4用户在使用本网站及服务时，须自行承担如下来自芦苇教育不可掌控的风险内容，包括但不限于：
        </p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;（1）教师与学员问或与任何第三人间的违约行为、侵权责任等，由有关当事人自行承担法律责任；
        </p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;（2）芦苇教育平台主张次结课时费，若教员与家长之间因协商非次结而导致的经济纠纷芦苇教育家教平台概不负责；
        </p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;（3）用户在使用本网站访问第三方网站时，因第三方网站及相关内容所可能导致的风险，由用户自行承担；
        </p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;（4）用户发布的内容被他人评论、转发、分享、复制、修改或做其他用途，因此等传播可能带来的风险和责任；
        </p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;（5）由于无线网络信号不稳定、无线网络带宽小等原因，所引起的本网站登录失败、资料同步不完整、页面打开速度慢等风险。
        </p>
        <br>
        <p>
            四、不可抗力及其他免责事由
        </p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;4.1您理解并同意，在使用本服务的过程中，可能会遇到不可抗力等风险因素，使本服务受到影响。不可抗力是指不能预见、不能克服并不能避免且对一方或双方造成重大影响的客观事件，包括但不限于自然灾害如洪水、地震、瘟疫流行和风暴等以及社会事件如战争、动乱、政府行为等。出现上述情况时，芦苇教育将努力在第一时间与相关单位配合，争取及时进行处理，但是由此给您造成的损失，芦苇教育在法律允许的范围内免责。
        </p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;4.2在法律允许的范围内，芦苇教育对以下情形导致的服务中断或受阻不承担责任：
        </p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;（1）受到计算机病毒、木马或其他恶意程序、黑客攻击的破坏。
        </p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;（2）用户或芦苇教育的电脑软件、系统、硬件和通信线路出现故障。
        </p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;（3）用户操作不当或用户通过非芦苇教育授权的方式使用本服务。
        </p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;（4）程序版本过时、设备的老化和/或其兼容性问题。
        </p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;（5）其他芦苇教育无法控制或合理预见的情形。
        </p>
        <br>
        <p>
            五、广告
        </p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;5.1您同意芦苇教育可以自行或由第三方通过短信、电子邮件或电子信息等多种方式向您发送、展示广告或其他信息（包括商业与非商业信息），广告或其他信息的具体发送及展示形式、频次及内容等以芦苇教育实际提供为准。
        </p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;5.2芦苇教育将依照相关法律法规要求开展广告业务。您同意，对本服务中出现的广告，您应审慎判断其真实性和可靠性，除法律明确规定外，您应对因该广告而实施的行为负责。
        </p>
        <br>
        <p>
            六、其它
        </p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;6.1您使用本网站和/或本服务即视为您已阅读本协议并接受本协议的约束。芦苇教育有权在必要时修改本协议条款。您可以在相关服务页面查阅最新版本的协议条款。本协议条款变更后，如果您继续使用芦苇教育提供的网站或服务，即视为您已接受变更后的协议。
        </p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;6.2本协议所有条款的标题仅为阅读方便，本身并无实际涵义，不能作为本协议涵义解释的依据。
        </p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;6.3本协议条款无论因何种原因部分无效或不可执行，其余条款仍有效，对双方具有约束力。
        </p>

        <br/>
    </div>
    <!--end右边-->
</div>
<!--end中间区域-->

<jsp:include page="../footer.jsp"></jsp:include>
