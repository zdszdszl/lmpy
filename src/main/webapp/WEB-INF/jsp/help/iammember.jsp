<%@ page language="java" import="java.util.*" pageEncoding="utf-8" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<jsp:include page="header.jsp"></jsp:include>


<dl class="w_1200 site_th">
    <dd>
        当前位置：
        <a href="javascript:void(0)">首页</a><span class="china">&gt;</span>我是学员
    </dd>
</dl>

<div class="help_center font_15 w_1200 white_bj">
    <div class="fl help_l">

        <ul class="help_nav nav_ul">
            <li class="">
                <a href="aboutUs">关于我们</a>
            </li>
            <li class="light">
                <a href="iammember">我是学员</a>
            </li>
            <li class="">
                <a href="iamteacher">我是教员</a>
            </li>
            <li class="">
                <a href="disclaimer">免责说明</a>
            </li>
            <li class="">
                <a href="announcement">活动公告</a>
            </li>
            <li class="">
                <a href="link">友情链接</a>
            </li>
        </ul>
    </div>

    <!--end左边-->
    <div class="r_cell help_r">
        <h4 class="th_line_c">
            我是学员
        </h4>
        <p style="margin-bottom: 0px; line-height: 26px; background: white;">
            <span style="font-size: 12px; font-family: 宋体; color: #555555">&nbsp;</span><span
                style="color: rgb(85, 85, 85); font-family: 宋体; font-size: 12px;">&nbsp;&nbsp;</span>
        </p>
        <p>
            <span style="font-family: Wingdings">Ø</span>学员如何请家教？
        </p>
        <br>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;学员可以通过网站免费发布家教请求。
        </p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;学员可以享有一定次数的免费直接查看联系教员。
        </p>
        <br>
        <p>
            <span style="font-family: Wingdings">Ø</span>学员如何支付课时费？
        </p>
        <br>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;上课前将费用通过平台支付即可。
        </p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;一般是一次一付，即上一次课支付一次的课时费用。如有特殊情况请与网站管理人员进行协商。
        </p>
        <br>
        <p>
            <span style="font-family: Wingdings">Ø</span>查看教员联系方式需要注意什么？
        </p>
        <br>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;注意教员登录时间，请联系近期登录的教员。
        </p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;长时间未登录的教员，建议不要联系，可能出现联系不到的情况。
        </p>
        <br>
        <p>
            <span style="font-family: Wingdings">Ø</span>如何上课？
        </p>
        <br>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;先与教员沟通上课需求，看教员是否可以满足。
        </p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;和教员沟通好上课方式、时间、地点等。
        </p>
        <br>
        <p>
            <span style="font-family: Wingdings">Ø</span>家教老师可以免费试课吗？
        </p>
        <br>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;提供免费试课的老师会在其简历上有所显示。
        </p>
        <br>

    </div>
    <!--end右边-->
</div>
<!--end中间区域-->

<jsp:include page="../footer.jsp"></jsp:include>
