<%@ page language="java" import="java.util.*" pageEncoding="utf-8" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<jsp:include page="header.jsp"></jsp:include>

<dl class="w_1200 site_th">
    <dd>
        当前位置：
        <a href="javascript:void(0)">首页</a><span class="china">&gt;</span>我是教员
    </dd>
</dl>

<div class="help_center font_15 w_1200 white_bj">
    <div class="fl help_l">

        <ul class="help_nav nav_ul">
            <li class="">
                <a href="aboutUs">关于我们</a>
            </li>
            <li class="">
                <a href="iammember">我是学员</a>
            </li>
            <li class="light">
                <a href="iamteacher">我是教员</a>
            </li>
            <li class="">
                <a href="disclaimer">免责说明</a>
            </li>
            <li class="">
                <a href="announcement">活动公告</a>
            </li>
            <li class="">
                <a href="link">友情链接</a>
            </li>
        </ul>
    </div>

    <!--end左边-->
    <div class="r_cell help_r">
        <h4 class="th_line_c">
            我是教员
        </h4>
        <p
                style="margin-bottom: 0; margin-bottom: 0; line-height: 26px; background: white">
			<span style="font-size: 12px; font-family: 宋体; color: #555555">&nbsp;
				&nbsp;</span>
        </p>
        <p>
            <span style="font-family: Wingdings">Ø</span>芦苇教育网注册教员收费吗？
        </p>
        <br>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;不收取任何注册费用。
        </p>
        <br>
        <p>
            <span style="font-family: Wingdings">Ø</span>做家教的流程是什么？
        </p>
        <br>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;教员先如实注册填写信息，经过网站审核后通过。
        </p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;注册成功后，教员可以选中能够接受的家教订单，联系学员进行自荐。
        </p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;教员也可以等待学员根据自身要求和各位教员的简历联系自己。
        </p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;联系成功后，双方可以沟通上课事宜。
        </p>
        <br>
        <p>
            <span style="font-family: Wingdings">Ø</span>被学员查看简历需要支付费用吗？
        </p>
        <br>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;不需要，同时被学员联系也不需要支付费用。
        </p>
        <br>
        <p>
            <span style="font-family: Wingdings">Ø</span>主动联系学员需要什么条件？
        </p>
        <br>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;不需要，主动联系学员不需要支付费用。
        </p>
        <br>
        <p>
            <span style="font-family: Wingdings">Ø</span>上课薪酬如何结算？
        </p>
        <br>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;一般为一次一结，网站会在授课结束后将相应的报酬支付给教员。特殊情况请与网站管理人员进行沟通协商。
        </p>
        <br>
        <p>
            <span style="font-family: Wingdings">Ø</span>如何快速接到家教订单？
        </p>
        <br>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;请确保自己的简历信息真实有效，尽可能地完善个人信息。接到订单后，积极沟通，认真授课，得到学员的认可与好评。
        </p>
        <br>

    </div>
    <!--end右边-->
</div>
<!--end中间区域-->


<jsp:include page="../footer.jsp"></jsp:include>
