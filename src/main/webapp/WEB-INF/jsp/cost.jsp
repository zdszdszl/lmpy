<%@ page language="java" import="java.util.*" pageEncoding="utf-8" %>
<%@page import="com.lmpy.lmpy.core.bean.GradePrice" %>
<%@page import="com.lmpy.lmpy.core.bean.Member" %>
<%@page import="com.lmpy.lmpy.core.bean.Teacher" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="renderer" content="webkit">
    <meta name="viewport" content="width=1200">

    <title>【芦苇教育】郑州家教_免费找家教_郑州家教一对一辅导</title>

    <link rel="icon" href="<%=basePath%>static/img/1.jpg" type="image/png">

    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/css/css11.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/css/code1.css">
    <LINK rel="stylesheet" type="text/css" href="<%=basePath%>static/css/base1.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/css/brief1.css">


    <script type="text/javascript" src="<%=basePath%>static/js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>static/js/jquery-ui.js"></script>
    <script type="text/javascript" src="<%=basePath%>static/js/tool.js"></script>
    <script>
        var _hmt = _hmt || [];
        (function () {
            var hm = document.createElement("script");
            hm.src = "https://hm.baidu.com/hm.js?20766a1a5bc02124297ce7be22ff84b1";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();
    </script>


</head>

<body>

<div class="header min_1200">
    <div class="w_1200">
        <img class="fl" src="<%=basePath%>static/img/1.jpg" width="190" height="100">
        <h2 class="fl logo_text">
            郑州市上门家教，来芦苇家教网！
        </h2>
        <div class="header_r fr">

            <%
                Teacher teacher = (Teacher) session.getAttribute("teacher");
                Member member = (Member) session.getAttribute("member");
                if (member == null && teacher == null) {
            %>
            <span class="font_13">
                    <img src="static/img/textlogo.png" width="160" height="80">

                    <b class="orange_link">
                        <a href="<%=basePath%>loginPage" class="heada">登录</a>
                    </b>
                    <em class="china"></em>
                    <b class="orange_link">
                        <a href="<%=basePath%>registerPage" class="heada">注册</a>
                    </b>
                    </span>

            <%} else { %>
            <span class="font_13">
                    <b class="orange_link">
                        <a href="<%=basePath%>logout" class="heada"> 退出</a>
                    </b>
                    <em class="china"></em>
                    <b class="orange_link">
                        <%
                            if (member != null) {
                        %>
	                       <a href="<%=basePath%>member/index" class="heada">我的</a>
	                   	<%} else if (teacher != null) { %>
	                       <a href="<%=basePath%>teacher/index" class="heada">我的</a>
	                   	<%} %>
                    </b>
                    </span>

            <%} %>

        </div>
        <!--end头文件右-->
    </div>
</div>
<!--end头文件-->

<!--end头文件-->

<div class="nav min_1200">
    <div class="tnav w_1200" id="slideNav">
        <span><a href="<%=basePath %>" class="current_nav">首页</a></span>
        <span><a href="<%=basePath %>costPage">资费标准</a></span>
        <span><a href="<%=basePath %>teacher/teachersPage">找老师</a></span>
        <span><a href="<%=basePath %>member/ordersPage">找家教</a></span>
        <span><a href="<%=basePath %>paper/paperPage" target="_blank">家教学堂</a></span>
        <span><a href="<%=basePath %>aboutUs" >必读指南</a></span>
        <i class="tavline" id="slideNavLine" style="width: 202px; left: 0px;"></i>
    </div>
</div>
<!-- end nav -->


<dl class="site_th w_1200">
    <dd>
        当前位置：
        <a href="<%=basePath%>" style=" text-decoration: none;">首页</a>
        <apan class="china">></apan>资费标准
    </dd>
</dl>


<div class="w_1200 i_center clearfix white_bj">
    <div class="prit_th png_img">
        <h2>芦苇教育家教网课时费标准</h2>
        <p><img src="<%=basePath %>static/img/tips_s.png" alt="" width="28" height="28">温馨提示：根据教员资质不同，价格会有浮动，具体以双方协商为准
        </p>
    </div>
    <!--end标题-->
    <div class="table_div">
        <table class="buy_table font_16">
            <thead align="center">
            <tr>
                <th width="30%">年级</th>
                <th width="30%" class="th_orange">大学生家教价格（元/小时）</th>
            </tr>
            <tr>
                <td>初一</td><td>50-100</td>
            </tr>
            <tr>
                <td>初二</td><td>55-100</td>
            </tr>
            <tr>
                <td>初三</td><td>60-100</td>
            </tr>
            <tr>
                <td>高一</td><td>60-120</td>
            </tr>
            <tr>
                <td>高二</td><td>70-120</td>
            </tr>
            <tr>
                <td>高三</td><td>80-120</td>
            </tr>
            </thead>
        </table>
    </div>
    <!--end一行-->
</div>
<!--end一行-->
<jsp:include page="footer.jsp"></jsp:include>