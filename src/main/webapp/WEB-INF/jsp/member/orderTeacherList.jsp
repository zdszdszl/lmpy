<%@ page language="java" import="java.util.*" pageEncoding="utf-8" %>
<%@ page import="com.lmpy.lmpy.core.bean.*" %>
<%@ page import="com.lmpy.lmpy.VO.OrderTeacherListVO" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";

    Member member = (Member) session.getAttribute("member");
    if (member == null) {
        response.sendRedirect(basePath);
        return;
    }
%>

<jsp:include page="header.jsp"></jsp:include>


<!--end一行的左-->
<div class="r_cell box_r">

    <dl class="tab_nav r_tab_nav">
        <dd>
            <a href="javascript:void(0)" class="tab_light">预约此家教的教员</a>
        </dd>
    </dl>
    <div class="@*tab_box*@ list_tb">
        <div>
            <table class="list_table hover_table">
                <thead>
                <tr>
                    <th>订单编号</th>
                    <th>授课科目</th>
                    <th>预约教员</th>
                    <th>预约状态</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                <%
                    List<OrderTeacherListVO> orderTeacherListVOS=(List<OrderTeacherListVO>)request.getAttribute("orderTeacherListVOS");
                    if (orderTeacherListVOS != null) {
                        for (int i = 0; i < orderTeacherListVOS.size(); i++) {
                            OrderTeacherListVO orderTeacherListVO = orderTeacherListVOS.get(i);
                %>
                <tr>
                    <td>

                        <%=orderTeacherListVO.getOrderCode()%>

                    </td>
                    <td><%=orderTeacherListVO.getCourseName() %>
                    </td>
                    <td data-id="63458" class="">
											<span class="blue_link">
												<a target="_blank"
                                                   href="../teacher/teacherInfoPage?tId=<%=orderTeacherListVO.getTeacherId() %>">
													<%=orderTeacherListVO.getTeacherName() %>
												</a>
											</span>
                    </td>

                    <td>
                        <%
                            int state = orderTeacherListVO.getStatus();
                            if (state == 22) {
                        %>
                        发布中
                        <%} else if (state == 23||state==24) { %>
                        教员申请
                        <%} else if (state == 25) { %>
                        未支付
                        <%} else if (state == 26) { %>
                        授课中
                        <%} else if (state == 27) { %>
                        已完成
                        <%} else if (state == 29) { %>
                        退款中
                        <%} else if (state == 30) { %>
                        退款完成
                        <%}%>

                    </td>
                    <td>
                        <%
                            if(state==23||state==24){
                                out.print("<span class=\"blue_link\">\n" +
                                        "\t                                    <a onclick=\"agreeTeacherAppoint("+orderTeacherListVO.getOID()+","+orderTeacherListVO.getTeacherId()+")\" _blank\"\n" +
                                        "                                           class=\"btndel\">同意申请</a>\n" +
                                        "\t                         \t       \t\t</span>");
                            }
                        %>

                    </td>
                </tr>
                <%
                        }
                    }
                %>
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
<!--end一行-->
<script>
    function agreeTeacherAppoint(oId,tId) {
        var d={
            oId:oId,
            tId:tId
        };
        $.post('agreeTeacherAppoint', d, function (res) {

            window.location="orderList";
        });
    }
</script>

<jsp:include page="../footer.jsp"></jsp:include>