<%@ page language="java" import="java.util.*" pageEncoding="utf-8" %>
<%@ page import="com.lmpy.lmpy.core.bean.*" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";

    Member member = (Member) session.getAttribute("member");
    if (member == null) {
        response.sendRedirect(basePath);
        return;
    }
%>
<script type="text/javascript" charset="utf-8" src="../static/back/Widget/ueditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="../static/back/Widget/ueditor/ueditor.all.min.js"> </script>
<!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
<!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
<script type="text/javascript" charset="utf-8" src="../static/back/Widget/ueditor/lang/zh-cn/zh-cn.js"></script>
<jsp:include page="header.jsp"></jsp:include>
<%
    Evaluation evaluation=(Evaluation) request.getAttribute("eval");
    Orders orders=(Orders) request.getAttribute("orders");
%>

<!--end一行的左-->
<div class="r_cell box_r">
<span class="formControls col-10">
								<script id="editor" type="text/plain" style="width:100%;height:400px; margin-left:10px;">
								</script>
							</span>

    <ul class="list-2 r" style="margin-top: 20px;margin-left: auto;margin-right: auto">
        <li>
            <a class="round_m png write_btn" onclick="article_save_submit();">保存并提交</a>
        </li>
    </ul>
</div>
</div>
<!--end一行-->

<script type="text/javascript">
    /**提交操作**/
    function article_save_submit(){
        var num=0;
        var str="";
            var d={
                oId:<%=orders.getoId()%>,
                content:UE.getEditor('editor').getContent()
            };

            $.post('updateEvaluation', d, function (res) {


            });


    }

    var ue = UE.getEditor('editor');

    ue.ready(function() {
        ue.setContent('<%=evaluation.getEvaluationContent()%>');
    });



</script>
<jsp:include page="../footer.jsp"></jsp:include>