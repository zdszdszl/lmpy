<%@ page language="java" import="java.util.*" pageEncoding="utf-8" %>
<%@ page import="com.lmpy.lmpy.core.bean.*" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";

    Member member = (Member) session.getAttribute("member");
    if (member == null) {
        response.sendRedirect(basePath);
        return;
    }
%>

<jsp:include page="header.jsp"></jsp:include>


<!--end一行的左-->
<div class="r_cell box_r">

    <dl class="tab_nav r_tab_nav">
        <dd>
            <a href="javascript:void(0)" class="tab_light">评价</a>
        </dd>
    </dl>
    <div class="@*tab_box*@ list_tb">
        <div>
            <table class="list_table hover_table">
                <thead>
                <tr>
                    <th>订单编号</th>
                    <th>教员</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                <%
                    List<Orders> orderList = (List<Orders>) request.getAttribute("orderList");
                    List<Evaluation> evaluations = (List<Evaluation>) request.getAttribute("evaluations");
                    if (orderList != null) {
                        for (int i = 0; i < orderList.size(); i++) {
                            Orders order = orderList.get(i);
                %>
                <tr>
                    <td>

                        <%=order.getOrderCode()%>

                    </td>
                    <td data-id="63458" class="">
											<span class="blue_link">
												<a target="_blank"
                                                   href="../teacher/teacherInfoPage?tId=<%=order.getTeacherId() %>">
													<%=order.getTeacherName() %>
												</a>
											</span>
                    </td>



                    <td>
                        <%
                            int state = order.getEvaluation();
                        %>

                        <%
                            if(state==0){
                                out.print("<span class=\"blue_link\">\n" +
                                        "\t                                    <a href=\"../member/toEvaluation?oId="+order.getoId()+"\" target=\"_blank\"\n" +
                                        "                                           class=\"btndel\">还未评价,去评价吧</a>\n" +
                                        "\t                         \t       \t\t</span>");
                            }
                            else if(state==1){
                                out.print("<span class=\"blue_link\">\n" +
                                        "\t                                    <a href=\"../member/toEvaluation?oId="+order.getoId()+"\" target=\"_blank\"\n" +
                                        "                                           class=\"btndel\">查看评价内容</a>\n" +
                                        "\t                         \t       \t\t</span>");
                            }
                        %>
                    </td>
                </tr>
                <%
                        }
                    }
                %>
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
<!--end一行-->

<script>
    function toPay(oId) {
        var d={
            oId:oId
        };
        $.post('toPay', d, function (res) {

            window.location="orderList";
        });
    }
    function finish(oId) {
        var d={
            oId:oId
        };
        $.post('finish', d, function (res) {

            window.location="orderList";
        });
    }
</script>
<jsp:include page="../footer.jsp"></jsp:include>