<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page language="java" import="java.util.*" pageEncoding="utf-8" %>
<%@page import="com.lmpy.lmpy.core.bean.Course" %>
<%@page import="com.lmpy.lmpy.core.bean.DictInfo" %>
<%@page import="com.lmpy.lmpy.core.bean.Teacher" %>
<%@page import="com.lmpy.lmpy.core.bean.Orders" %>
<%@page import="com.lmpy.lmpy.core.bean.Member" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    Member member = (Member) session.getAttribute("member");
    if (member == null) {
        response.sendRedirect(basePath);
        return;
    }
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="renderer" content="webkit">
    <meta name="viewport" content="width=1200">

    <title>【芦苇教育】郑州家教_免费找家教_郑州家教一对一辅导</title>

    <link rel="icon" href="<%=basePath%>static/img/1.jpg" type="image/png">

    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/css/css11.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/css/code1.css">
    <LINK rel="stylesheet" type="text/css" href="<%=basePath%>static/css/base1.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/css/brief1.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/css/pageSwitch.min1.css">

    <script type="text/javascript" src="<%=basePath%>static/js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>static/js/jquery-ui.js"></script>
    <script type="text/javascript" src="<%=basePath%>static/js/tool.js"></script>
    <script type="text/javascript" src="<%=basePath%>static/js/code.js"></script>
    <script src="<%=basePath%>static/js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="<%=basePath%>static/js/jquery.artDialog.min.js" type="text/javascript"></script>
    <script src="<%=basePath%>static/js/jQuery.rTabs.js" type="text/javascript"></script>
    <script src="<%=basePath%>static/js/search_tab.js" type="text/javascript"></script>

    <script>
        var _hmt = _hmt || [];
        (function () {
            var hm = document.createElement("script");
            hm.src = "https://hm.baidu.com/hm.js?20766a1a5bc02124297ce7be22ff84b1";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();
    </script>

</head>

<body>

<div class="header min_1200">
    <div class="w_1200">
        <img class="fl" src="<%=basePath%>static/img/logo_contact1.png" width="190" height="100">
        <h2 class="fl logo_text">
            郑州市上门家教，来芦苇家教网！
        </h2>

        <div class="header_r fr">


            <span class="font_13">
                <img src="<%=basePath%>static/img/textlogo.png" width="160" height="80">

                    <b class="orange_link">
                        <a href="<%=basePath%>logout" class="heada"> 退出</a>
                    </b>
                    <em class="china"></em>
                    <b class="orange_link">
                        <a href="<%=basePath%>member/index" class="heada">我的</a>
                    </b>
                    </span>


        </div>
        <!--end头文件右-->
    </div>
</div>
<!--end头文件-->

<!--end头文件-->

<div class="nav min_1200">
    <div class="tnav w_1200" id="slideNav">
        <span><a href="<%=basePath %>" class="current_nav">首页</a></span>
        <span><a href="<%=basePath %>costPage">资费标准</a></span>
        <span><a href="<%=basePath %>teacher/teachersPage">找老师</a></span>
        <span><a href="<%=basePath %>member/ordersPage">找家教</a></span>
        <span><a href="<%=basePath %>paper/paperPage" target="_blank">家教学堂</a></span>
        <span><a href="<%=basePath %>aboutUs" >必读指南</a></span>
        <i class="tavline" id="slideNavLine" style="width: 202px; left: 0px;"></i>
    </div>
</div>
<!-- end nav -->


<div class="tearch_banner min_1200 studen_banner">
    <div class="w_1200">
        <dl class="studen_banner_dl png_img">
            <dt class="fl">
                <h4>

                    <a href="javascript:void(0);" class="email_a"> <span
                            class="f_num">0</span> <img
                            src="<%=basePath%>static/img/email.png" alt="" width="30"
                            height="30"> </a>
                </h4>
                <div class="t">
                    <span class="phone_a"><%=member.getPhone() %></span><span class="mt_40"></span>积分：
                    <b class="orange_text"><%=member.getMoney()%></b>
                </div>
            </dt>
            <dd class="r_cell">
                <ul class="list_ul gray_link gray_text">

                </ul>
                <div class="btn_r">
                    <a href="<%=basePath %>member/publishOrderPage"
                       class="round_m png write_btn">立即发布需求</a>
                </div>
            </dd>
        </dl>
    </div>
</div>
<!--end一行-->
<!-- end nav -->
<div class="w_1200 d_center white_bj">
    <div class="fl w_300 box_line">
        <dl class="tearch_l_nav">
            <dt><span>快捷操作</span></dt>
            <dd>
                <div class="t clearfix">
                    <h5>信息更新</h5>
                    <ul class="list_2">
                        <li><a href="<%=basePath %>member/index">我的信息</a></li>
                        <li><a href="<%=basePath %>member/updateMemberPage">修改信息</a></li>
                        <li><a href="<%=basePath %>member/updatepwdPage">修改密码</a></li>
                        <!--<li><a href="<%=basePath %>member/PayPage">充值</a></li>-->
                        <!--<li><a href="<%=basePath %>teacher/idimgPage">身份认证</a></li>-->
                    </ul>
                </div>

                <div class="t clearfix">
                    <h5>家教情况</h5>
                    <ul class="list_2">
                        <li><a href="<%=basePath %>member/orderList">我的家教订单</a></li>
                        <li><a href="<%=basePath %>member/appointList">我预约的教员</a></li>
                        <li><a href="<%=basePath %>member/evaluation">学员评价</a></li>

                    </ul>
                </div>


            </dd>
        </dl>
    </div>
    <!--end一行的左-->
