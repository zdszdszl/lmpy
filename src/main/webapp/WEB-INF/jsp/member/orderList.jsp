<%@ page language="java" import="java.util.*" pageEncoding="utf-8" %>
<%@page import="com.lmpy.lmpy.core.bean.Member" %>
<%@page import="com.lmpy.lmpy.core.bean.District" %>
<%@page import="com.lmpy.lmpy.core.bean.MemberOrderTeacher" %>
<%@page import="com.lmpy.lmpy.core.bean.Orders" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";

    Member member = (Member) session.getAttribute("member");
    if (member == null) {
        response.sendRedirect(basePath);
        return;
    }
%>

<jsp:include page="header.jsp"></jsp:include>


<!--end一行的左-->
<div class="r_cell box_r">

    <dl class="tab_nav r_tab_nav">
        <dd>
            <a href="javascript:void(0)" class="tab_light">我的家教订单</a>
        </dd>
    </dl>
    <div class="@*tab_box*@ list_tb">
        <div>
            <table class="list_table hover_table">
                <thead>
                <tr>
                    <th>订单编号</th>
                    <th>授课科目</th>
                    <th>授课教员</th>
                    <th>发布时间</th>
                    <th>预约状态</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                <%
                    List<Orders> orderList = (List<Orders>) request.getAttribute("orderList");
                    if (orderList != null) {
                        for (int i = 0; i < orderList.size(); i++) {
                            Orders order = orderList.get(i);
                %>
                <tr>
                    <td>

                        <%=order.getOrderCode()%>

                    </td>
                    <td><%=order.getCourseName() %>
                    </td>
                    <td data-id="63458" class="">
											<span class="blue_link">
												<a target="_blank"
                                                   href="../teacher/teacherInfoPage?tId=<%=order.getTeacherId() %>">
													<%=order.getTeacherName() %>
												</a>
											</span>
                    </td>
                    <td>
											<span class="data_2">
	                                    		<%=order.getPublicTime().substring(0, 10) %>
	                                    		<p><%=order.getPublicTime().substring(10, 18) %></p>
	                                		</span>
                    </td>
                    <td>
                        <%
                            int state = order.getOrderStatus();
                            if (state == 22) {
                        %>
                        发布中
                        <%} else if (state == 23||state==24) { %>
                        教员申请
                        <%} else if (state == 25) { %>
                        未支付
                        <%} else if (state == 26) { %>
                        授课中
                        <%} else if (state == 27) { %>
                        已完成
                        <%} else if (state == 29) { %>
                        退款中
                        <%} else if (state == 30) { %>
                        退款完成
                        <%}%>
                    </td>
                    <td>
											<span class="blue_link">
	                                    <a href="../order/orderInfo?oId=<%=order.getoId() %>" target="_blank"
                                           class="btndel">查看</a>
	                         	       		</span>
                        <%
                            if(state==23||state==24){
                                out.print("<span class=\"blue_link\">\n" +
                                        "\t                                    <a href=\"../member/listOrderTeachers?oId="+order.getoId()+"\" target=\"_blank\"\n" +
                                        "                                           class=\"btndel\">查看申请此订单的教员</a>\n" +
                                        "\t                         \t       \t\t</span>");
                            }
                            else if(state==25){
                                out.print("<span class=\"blue_link\">\n" +
                                        "\t                                    <a onclick=toPay("+order.getoId()+") target=\"_blank\"\n" +
                                        "                                           class=\"btndel\">去支付</a>\n" +
                                        "\t                         \t       \t\t</span>");
                            }
                            else if(state==26){
                                out.print("<span class=\"blue_link\">\n" +
                                        "\t                                    <a onclick=finish("+order.getoId()+") target=\"_blank\"\n" +
                                        "                                           class=\"btndel\">确认授课完成</a>\n" +
                                        "\t                         \t       \t\t</span>");
                            }
                        %>
                    </td>
                </tr>
                <%
                        }
                    }
                %>
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
<!--end一行-->

<script>
    function toPay(oId) {
        var d={
            oId:oId
        };
        $.post('toPay', d, function (res) {

            window.location="orderList";
        });
    }
    function finish(oId) {
        var d={
            oId:oId
        };
        $.post('finish', d, function (res) {

            window.location="orderList";
        });
    }
</script>
<jsp:include page="../footer.jsp"></jsp:include>