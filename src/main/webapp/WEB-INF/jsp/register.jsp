<%@ page language="java" import="java.util.*" pageEncoding="utf-8" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">

    <title>【芦苇教育】郑州家教_免费找家教_郑州家教一对一辅导</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="<%=basePath%>static/img/1.jpg" type="image/png">

    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/css/css11.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/css/code1.css">
    <script src="<%=basePath%>static/js/jquery-1.7.2.min.js"></script>
    <script src="<%=basePath%>static/js/jquery-ui.js"></script>

    <script src="<%=basePath%>static/js/yg_ac_utils.js"></script>

    <script src="<%=basePath%>static/js/jquery.placeholder.min.js"></script>
    <script src="<%=basePath%>static/js/jquery.cookie.min.js"></script>
    <script src="<%=basePath%>static/js/register.js"></script>
    <style>
        .errorMessage {
            clear: both;
            display: none;
            padding-left: 23px;
            height: 32px;
            font: 12px/ 32px "SimSun";
            color: #db0101;
            background: url(<%=basePath%>static/img/error.png) left center no-repeat;
        }

    </style>

    <script type="text/javascript" src="<%=basePath%>static/js/tool.js"></script>

    <script type="text/javascript" src="<%=basePath%>static/js/code.js"></script>

    <script>
        var _hmt = _hmt || [];
        (function () {
            var hm = document.createElement("script");
            hm.src = "https://hm.baidu.com/hm.js?20766a1a5bc02124297ce7be22ff84b1";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();
    </script>


</head>

<body>
<div class="header min_1200">
    <div class="w_1200">
        <img class="fl" src="<%=basePath%>static/img/1.jpg" width="190" height="100">
        <h2 class="fl logo_text">
            郑州市上门家教，来芦苇家教网！
        </h2>
        <div class="header_r fr">
            <img src="static/img/textlogo.png" width="160" height="80">
        </div>
    </div>
</div>
<div class="nav min_1200">
    <div class="tnav w_1200" id="slideNav">
        <span><a href="<%=basePath %>" class="current_nav">首页</a></span>
        <span><a href="<%=basePath %>costPage">资费标准</a></span>
        <span><a href="<%=basePath %>teacher/teachersPage">找老师</a></span>
        <span><a href="<%=basePath %>member/ordersPage">找家教</a></span>
        <span><a href="<%=basePath %>paper/paperPage" target="_blank">家教学堂</a></span>
        <span><a href="<%=basePath %>aboutUs" >必读指南</a></span>
        <i class="tavline" id="slideNavLine" style="width: 202px; left: 0px;"></i>
    </div>
</div>
<dl class="site_th w_1200">
    <dd>
        当前位置：
        <a href="<%=basePath%>" style=" text-decoration: none;">首页</a>
        <apan class="china">></apan>注册
    </dd>
</dl>
<!--end头文件-->

<div class="w_1040 clearfix">
    <div class="fr w_410">
        <dl class="tab_nav reg_tab_nav_n">
            <dd class="do-ajax">
                <a href="javascript:void(0)">家长注册</a>
                <a href="javascript:void(0)">教员注册</a>
            </dd>
        </dl>
        <div class="login_fill @*tab_box*@">
            <div id="mobilereg">
                <div class="login_o">
                    <div class="login_div">
                        <label class="fl" for="mobilephone">
                            手机号码
                        </label>
                        <div class="r_cell">
                            <input type="text" class="login_text" id="mobilephone"
                                   name="mobilephone" placeholder="请输入您的手机号">
                        </div>
                        <p class="errorMessage"></p>
                    </div>
                </div>

                <div class="login_o">
                    <div class="login_div">
                        <label class="fl" for="Password">
                            设置密码
                        </label>
                        <div class="r_cell">
                            <input type="password" class="login_text password"
                                   id="Password" name="Password"
                                   placeholder="密码长度6~16位，数字、字母、字符等">
                        </div>
                        <p class="errorMessage"></p>
                    </div>
                </div>


                <div class="login_o">
                    <div class="login_div">
                        <label class="fl" for="cfmPassword">
                            确认密码
                        </label>
                        <div class="r_cell">
                            <input type="password" class="login_text password"
                                   id="cfmPassword" name="cfmPassword" placeholder="再次输入密码">
                        </div>
                        <p class="errorMessage"></p>
                    </div>
                </div>

                <div class="login_o">
                    <div class="login_div">
								<span class="login_ma orange_link">
									<a href="javascript:void(0)">
										<img id="codeImg" alt="验证码" src="<%=basePath %>code"
                                             onclick="changeImg()" title="看不清？点击换一张"/>
									</a>
								</span>
                        <label class="fl" for="imgvefyData">
                            图形验证码
                        </label>
                        <div class="r_cell">
                            <input type="text" class="login_text" id="imgvefyData"
                                   name="imgvefyData" placeholder="请输入图形验证码">
                        </div>
                        <p class="errorMessage"></p>
                    </div>
                </div>

                <div class="login_o_l orange_link">

                    <div class="treaty">
                        <input type="checkbox" id="agree" name="agree" value="1"
                               checked="checked" class="check">
                        已阅读并同意
                        <a href="javascript:void(0);" rel="nofollow"
                           target="_blank">《用户协议》</a>和
                        <a class="policy1" href="javascript:void(0);"
                           rel="nofollow" style="" target="_blank">《学员须知》</a>
                        <a class="policy2" href="javascript:void(0);"
                           rel="nofollow" style="display: none;" target="_blank">《教员须知》</a>
                    </div>
                    <p class="errorMessage"></p>
                </div>

                <div class="login_btn_div">
                    <button type="submit" class="login_btn round_m transition_a"
                            id="mobireg">
                        注 册
                    </button>
                </div>

            </div>
        </div>
        <!--end右边_标签内容-->
    </div>
    <!--end右边-->
    <div class="fl">
        <img src="<%=basePath %>static/img/reedlogo.png"
             alt=""  width="400" height="400">
    </div>
    <!--end左边-->
</div>
<!--end中间区域-->

<div class="min_1200 footer_i clearfix">
    <div class="w_1200">
				<span class="fr"><a href="javascript:void(0)">XX安备 XXXXXXXXXX号</a>
				</span><span class="fl">版权所有 XXXXXXXXXXX有限公司－芦苇家教网 <a
            href="javascript:void(0)">xx备xxxxxxxx号</a>
				</span>
    </div>
</div>
<!--end文件底-->

</body>
</html>

