<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="renderer" content="webkit">
    <meta name="viewport" content="width=1200">

    <title>【芦苇教育】郑州家教_免费找家教_郑州家教一对一辅导</title>

    <link rel="icon" href="<%=basePath%>static/img/1.jpg" type="image/png">


    <!--<link rel="stylesheet" type="text/css" href="<%=basePath%>static/css/css.css">-->
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/css/css11.css">
    <script src="<%=basePath%>static/js/jquery-1.9.1.min.js"></script>
    <script src="<%=basePath%>static/js/jquery-ui.js"></script>

    <script type="text/javascript" src="<%=basePath%>static/js/tool.js"></script>

    <!--<link rel="stylesheet" type="text/css" href="<%=basePath%>static/css/code.css">-->
    <script type="text/javascript" src="<%=basePath%>static/js/code.js"></script>
    <script>
        var _hmt = _hmt || [];
        (function () {
            var hm = document.createElement("script");
            hm.src = "https://hm.baidu.com/hm.js?20766a1a5bc02124297ce7be22ff84b1";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();
    </script>


</head>

<body>
<div class="header min_1200">
    <div class="w_1200">
        <img class="fl" src="<%=basePath%>static/img/1.jpg" width="190" height="100">
        <h2 class="fl logo_text">
            郑州市上门家教，来芦苇家教网！
        </h2>
        <div class="header_r fr">
            <img src="static/img/textlogo.png" width="160" height="80">
        </div>
    </div>
</div>

<div class="nav min_1200">
    <div class="tnav w_1200" id="slideNav">
        <span><a href="<%=basePath %>" class="current_nav">首页</a></span>
        <span><a href="<%=basePath %>costPage">资费标准</a></span>
        <span><a href="<%=basePath %>teacher/teachersPage">找老师</a></span>
        <span><a href="<%=basePath %>member/ordersPage">找家教</a></span>
        <span><a href="<%=basePath %>paper/paperPage" target="_blank">家教学堂</a></span>
        <span><a href="<%=basePath %>aboutUs" >必读指南</a></span>
        <i class="tavline" id="slideNavLine" style="width: 202px; left: 0px;"></i>
    </div>
</div>
<!-- end导航-->

<dl class="site_th w_1200">
    <dd>
        当前位置：
        <a href="<%=basePath%>" style=" text-decoration: none;">首页</a>
        <apan class="china">></apan>登录
    </dd>
</dl>

<style>
    .wraploginErrorMessage {
        height: 25px;
        width: 297px;
        opacity: 1;
        margin-left: 30px;
        margin-bottom: 7px;
        position: relative;
    }

    .loginErrorMessage {
        height: 25px;
        line-height: 25px;
        font-size: 16px;
        color: red;
        background: #feecec;
        border: 1px solid #fe999f;
        text-indent: 1em;
        position: absolute;
        top: 0;
        left: 0;
        width: 360px;
    }
</style>

<div class="w_1040 clearfix login_center">
    <div class="fr w_410">
        <dl class="tab_nav reg_tab_nav_n">
            <dd class="do-ajax">
                <a href="javascript:void(0)" >家长登录</a>
                <a href="javascript:void(0)" >教员登录</a>
            </dd>
        </dl>
        <div id="wraploginErrorMessage" class="wraploginErrorMessage"></div>

        <div class="login_o">
            <div class="login_div">
                <label class="fl" for="UserName">
                    手机号
                </label>
                <div class="r_cell">
                    <input type="text" class="login_text" id="UserName"
                           name="UserName" placeholder="请输入手机号" autocomplete="off">
                </div>
            </div>
        </div>

        <div class="login_o">
            <div class="login_div">
                <label class="fl" for="Password">
                    密码
                </label>
                <div class="r_cell">
                    <input type="password" class="login_text" id="Password"
                           name="Password" placeholder="请输入您的密码">
                </div>
            </div>
        </div>

        <div class="link_line orange_link">
            <span style="margin-left: 167px;">
                <a href="<%=basePath %>registerPage">免费注册</a>&nbsp;&nbsp;
                <a href="#">忘记密码</a>
            </span>
        </div>

        <div class="login_btn_div">
            <input type="button" id="login_btn"
                   class="login_btn round_m transition_a" value="立 即 登 录">
        </div>
    </div>
    <!--end右边-->
    <div class="fl"><!--图标logo-->
        <img src="<%=basePath %>static/img/reedlogo.png"
             alt=""  width="400" height="400">
    </div>
    <!--end左边-->
</div>
<!--end中间区域-->

<!--end文件底-->

<script src="<%=basePath%>static/js/jquery.cookie.min.js"></script>
<script src="<%=basePath%>static/js/jquery.placeholder.min.js"></script>
<script src="<%=basePath%>static/js/ac_login.js"></script>

<div class="min_1200 footer_i clearfix">
    <div class="w_1200">
				<span class="fr"><a href="javascript:void(0)">XX安备 XXXXXXXXXX号</a>
				</span><span class="fl">版权所有 XXXXXXXXXXX有限公司－芦苇家教网 <a
            href="javascript:void(0)">XXXXXXXX号</a>
				</span>
    </div>
</div>

</body>
</html>