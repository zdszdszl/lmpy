<%@ page language="java" import="java.util.*" pageEncoding="utf-8" %>
<%@page import="com.lmpy.lmpy.core.bean.Course" %>
<%@page import="com.lmpy.lmpy.core.bean.DictInfo" %>
<%@page import="com.lmpy.lmpy.core.bean.Teacher" %>
<%@page import="com.lmpy.lmpy.core.bean.Orders" %>
<%@page import="com.lmpy.lmpy.core.bean.Member" %>
<%@page import="com.lmpy.lmpy.core.bean.Link" %>
<%@page import="com.lmpy.lmpy.core.bean.Paper" %>
<%@page import="com.lmpy.lmpy.core.bean.News" %>
<%@page import="com.lmpy.lmpy.core.bean.Notice" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="renderer" content="webkit">
    <meta name="viewport" content="width=1300">

    <meta name="360-site-verification" content="22009208546d72dc927d8f886d5d0bb1"/>

    <title>【芦苇教育】郑州家教_免费找家教_郑州家教一对一辅导</title>

    <link rel="icon" href="<%=basePath%>static/img/1.jpg" type="image/png">

    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/css/css11.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/css/code1.css">
    <LINK rel="stylesheet" type="text/css" href="<%=basePath%>static/css/base1.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/css/brief1.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/css/pageSwitch.min1.css">

    <script type="text/javascript" src="<%=basePath%>static/js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>static/js/jquery-ui.js"></script>
    <script type="text/javascript" src="<%=basePath%>static/js/tool.js"></script>
    <script type="text/javascript" src="<%=basePath%>static/js/code.js"></script>
    <script src="<%=basePath%>static/js/jquery.artDialog.min.js" type="text/javascript"></script>
    <script src="<%=basePath%>static/js/jQuery.rTabs.js" type="text/javascript"></script>
    <script src="<%=basePath%>static/js/search_tab.js" type="text/javascript"></script>


    <STYLE>
        * {
            padding: 0;
            margin: 0;
        }

        html,
        body {
            height: 100%;
        }

        #container {
            width: 100%;
            height: 500px;
            overflow: hidden;
        }

        .sections,
        .section {
            height: 100%;
        }

        #container,
        .sections {
            position: relative;
        }

        .section {
            background-color: #000;
            background-size: cover;
            background-position: 50% 50%;
            text-align: center;
            color: white;
        }

        #section0 {
            background-image: url('<%=basePath%>static/img/banner (1).png');
        }

        #section1 {
            background-image: url('<%=basePath%>static/img/banner.png');
        }

        #section2 {
            background-image: url('<%=basePath%>static/img/01.jpg');
        }

        .mainProNav {
            background: rgba(0, 0, 0, .7);
            width: 230px;
            height: 500px;
        }

        .mainProNav dt {
            color: #fff;
        }

        .mainProNav dt {
            padding-top: 15px;
            height: 80px;
            line-height: 18px;
            font-size: 16px;
        }

        .mainProNav .dlHover dt {
            padding-left: 24px;
            background: #000;
            line-height: 18px;
            padding-top: 15px;
            height: 80px;
        }

        .mainProNav p {
            height: 28px;
            padding-top: 10px;
            font-size: 12px;
            font-weight: 100;
            width: 180px;
        }

        .mainProNav dd {
            left: 230px;
            border-left: none
        }
    </STYLE>
    <script>
        var _hmt = _hmt || [];
        (function () {
            var hm = document.createElement("script");
            hm.src = "https://hm.baidu.com/hm.js?20766a1a5bc02124297ce7be22ff84b1";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();
    </script>
</head>

<body>

<div class="header min_1200">
    <div class="w_1200">
        <img class="fl" src="<%=basePath%>static/img/1.jpg" width="190" height="100">
        <h2 class="fl logo_text">
            郑州市上门家教，来芦苇家教网！
        </h2>
        <div class="header_r fr">

            <%
                Teacher teacher = (Teacher) session.getAttribute("teacher");
                Member member = (Member) session.getAttribute("member");
                if (member == null && teacher == null) {
            %>
            <span class="font_13">
                    <img src="static/img/textlogo.png" width="160" height="80">

                    <b class="orange_link">
                        <a href="<%=basePath%>loginPage" class="heada">登录</a>
                    </b>
                    <em class="china"></em>
                    <b class="orange_link">
                        <a href="<%=basePath%>registerPage" class="heada">注册</a>
                    </b>
                    </span>

            <%} else { %>
            <span class="font_13">
                    <b class="orange_link">
                        <a href="<%=basePath%>logout" class="heada"> 退出</a>
                    </b>
                    <em class="china"></em>
                    <b class="orange_link">
                        <%
                            if (member != null) {
                        %>
	                       <a href="<%=basePath%>member/index" class="heada">我的</a>
	                   	<%} else if (teacher != null) { %>
	                       <a href="<%=basePath%>teacher/index" class="heada">我的</a>
	                   	<%} %>
                    </b>
                    </span>

            <%} %>

        </div>
        <!--end头文件右-->
    </div>
</div>
<!--end头文件-->

    <div class="nav min_1200">
    <div class="tnav w_1200" id="slideNav">
        <span><a href="<%=basePath %>" class="current_nav">首页</a></span>
        <span><a href="<%=basePath %>costPage">资费标准</a></span>
        <span><a href="<%=basePath %>teacher/teachersPage">找老师</a></span>
        <span><a href="<%=basePath %>member/ordersPage">找家教</a></span>
        <span><a href="<%=basePath %>paper/paperPage" target="_blank">家教学堂</a></span>
        <span><a href="<%=basePath %>aboutUs">必读指南</a></span>
        <i class="tavline" id="slideNavLine" style="width: 202px; left: 0px;"></i>
    </div>
</div>

<!--end nav-->
<!--轮播图begin-->
<div id="container">
    <div class="sections">

        <div class="section" id="section1">
        </div>
        <div class="section" id="section2">
        </div>
    </div>
</div>

<script src="<%=basePath%>static/js/pageSwitch.min.js"></script>
<script>
    $("#container").PageSwitch({
        direction: 'horizontal',
        easing: 'ease-in',
        duration: 1000,
        autoPlay: true,
        loop: 'false'
    });
</script>
<!--轮播图end-->

<div class="banner_fill_box w_1200">

    <!--设置找老师-->




    <!--设置找老师end-->


</div>

<!--热门大学、热门学科、热门区域-->
<div class="gray_border clearfix w_1200 box_w">
    <div class="hide_box">
        <%
            List<Notice> topFourNoticeTypeFirstList = (List<Notice>) request.getAttribute("topFourNoticeTypeFirstList");
            List<Notice> topFourNoticeTypeSecondList = (List<Notice>) request.getAttribute("topFourNoticeTypeSecondList");
            List<Notice> topFourNoticeTypeThirdList = (List<Notice>) request.getAttribute("topFourNoticeTypeThirdList");
        %>
        <ul class="course_ul clearfix">
            <li class="li_o">
                <h3><a href="<%=basePath %>notice/conNoticeList?type=1">通知公告</a></h3>
                <ul>
                    <%
                        if (topFourNoticeTypeFirstList != null) {
                            for (int i = 0; i < topFourNoticeTypeFirstList.size(); i++) {
                                Notice notice = topFourNoticeTypeFirstList.get(i);
                    %>
                    <li>
                        <a href="<%=basePath %>notice/noticeInfo?nId=<%=notice.getnId() %>"
                           target="_blank"><%=notice.getTitle() %>
                        </a>
                        <%
                            i = i + 1;
                            notice = topFourNoticeTypeFirstList.get(i);

                        %>
                        <p>
                            <a href="<%=basePath %>notice/noticeInfo?nId=<%=notice.getnId() %>"
                               target="_blank"><%=notice.getTitle() %>
                            </a>
                        </p>

                    </li>
                    <%
                            }
                        }
                    %>
                </ul>
            </li>
            <li class="li_o">
                <h3><a href="<%=basePath %>notice/conNoticeList?type=2">学员必读</a></h3>
                <ul>
                    <%
                        if (topFourNoticeTypeSecondList != null) {
                            for (int i = 0; i < topFourNoticeTypeSecondList.size(); i++) {
                                Notice notice = topFourNoticeTypeSecondList.get(i);
                    %>
                    <li>
                        <a href="<%=basePath %>notice/noticeInfo?nId=<%=notice.getnId() %>"
                           target="_blank"><%=notice.getTitle() %>
                        </a>
                        <%
                            i = i + 1;
                            notice = topFourNoticeTypeSecondList.get(i);

                        %>
                        <p>
                            <a href="<%=basePath %>notice/noticeInfo?nId=<%=notice.getnId() %>"
                               target="_blank"><%=notice.getTitle() %>
                            </a>
                        </p>

                    </li>
                    <%
                            }
                        }
                    %>
                </ul>
            </li>

            <li class="li_o last">
                <h3><a href="<%=basePath %>notice/conNoticeList?type=3">教员必读</a></h3>
                <ul>
                    <%
                        if (topFourNoticeTypeThirdList != null) {
                            for (int i = 0; i < topFourNoticeTypeThirdList.size(); i++) {
                                Notice notice = topFourNoticeTypeThirdList.get(i);
                    %>
                    <li>
                        <a href="<%=basePath %>notice/noticeInfo?nId=<%=notice.getnId() %>"
                           target="_blank"><%=notice.getTitle() %>
                        </a>
                        <%
                            i = i + 1;
                            notice = topFourNoticeTypeThirdList.get(i);

                        %>
                        <p>
                            <a href="<%=basePath %>notice/noticeInfo?nId=<%=notice.getnId() %>"
                               target="_blank"><%=notice.getTitle() %>
                            </a>
                        </p>

                    </li>
                    <%
                            }
                        }
                    %>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!--end热门大学、热门学科、热门区域-->

<div class="h_tab w_1200 clearfix box_w_n">
    <dl class="tab_nav_w h_tab_nav">
        <dd class="fr">
            <a href="<%=basePath%>teacher/teachersPage"
               class="round_m transition_a more_a">更多+&nbsp;&nbsp;&nbsp;</a>
        </dd>
        <dt class="fl">
            <h4>中小学文化课</h4>
        </dt>
    </dl>
    <div class="gray_border clearfix tab_box_o">
        <div class="course_class">
            <dl class="course_class_dl">
                <dt class="clearfix">
                    		<span class="h_arrow png">
                    		</span>
                    <ul class="list_2 clearfix">
                        <li><a href="teacher/teacherListPage?areaId=0&teacherType=0&gender=2&course=66">小学陪读</a></li>
                        <li><a href="teacher/teacherListPage?areaId=0&teacherType=0&gender=2&course=65">学前教育</a></li>
                        <li><a href="teacher/teacherListPage?areaId=0&teacherType=0&gender=2&course=65">学龄前课..</a></li>
                        <li><a href="teacher/teacherListPage?areaId=0&teacherType=0&gender=2&course=66">小学课程</a></li>
                        <li><a href="teacher/teacherListPage?areaId=0&teacherType=0&gender=2&course=68">初中课程</a></li>
                        <li><a href="teacher/teacherListPage?areaId=0&teacherType=0&gender=2&course=70">高中课程</a></li>
                        <li><a href="teacher/teacherListPage?areaId=0&teacherType=0&gender=2&course=69">初三数学</a></li>
                        <li><a href="teacher/teacherListPage?areaId=0&teacherType=0&gender=2&course=69">初三英语</a></li>
                        <li><a href="teacher/teacherListPage?areaId=0&teacherType=0&gender=2&course=69">初三语文</a></li>
                        <li><a href="teacher/teacherListPage?areaId=0&teacherType=0&gender=2&course=71">高三数学</a></li>
                        <li><a href="teacher/teacherListPage?areaId=0&teacherType=0&gender=2&course=71">高三英语</a></li>
                        <li><a href="teacher/teacherListPage?areaId=0&teacherType=0&gender=2&course=71">高三语文</a></li>
                        <li><a href="teacher/teacherListPage?areaId=0&teacherType=0&gender=2&course=71">高三物理</a></li>
                        <li><a href="teacher/teacherListPage?areaId=0&teacherType=0&gender=2&course=71">高三化学</a></li>
                    </ul>
                </dt>
                <dd>
                    <a href="<%=basePath%>teacher/teachersPage">更多课程<span class="china">&gt;&gt;</span></a>
                </dd>
            </dl>
        </div>
        <!--end分类-->
        <div class="tab_box_w r_cell h_tab_box_w">
            <div class="hide_box" style="display: block;">
                <ul class="list_3 tearch_ul round_img">
                    <%
                        //中小学教员信息列表
                        List<Teacher> zhongXiaoXueTeacherList = (List<Teacher>) request
                                .getAttribute("zhongXiaoXueTeacherList");
                        if (zhongXiaoXueTeacherList != null) {
                            for (int i = 0; i < zhongXiaoXueTeacherList.size(); i++) {
                                Teacher t = zhongXiaoXueTeacherList.get(i);
                    %>
                    <li class="li_o">
                        <a href="<%=basePath%>teacher/teacherInfoPage?tId=<%=t.getTeacherId()%>" class="box">
                            <div class="hover_div transition_a">
                                <p class="hide2">
                                    <%
                                        if (t.getSelfEvaluation().length() > 50) {
                                    %>
                                    <%=t.getSelfEvaluation().substring(0, 50)%>...
                                    <%
                                    } else {
                                    %>
                                    <%=t.getSelfEvaluation()%>
                                    <%
                                        }
                                    %>
                                </p>
                                <div class="d">
                                    <span class="look_btn transition_a">查看详情</span>
                                </div>
                            </div>
                            <dl>
                                <dt class="fl">
                                    <%
                                        if (t.getGender() == 0 && "".equals(t.getPersonImg())) {
                                    %>
                                    <img src="<%=basePath%>static/img/w.png" alt="" width="100" height="100">
                                    <%
                                    } else if (t.getGender() == 1
                                            && "".equals(t.getPersonImg())) {
                                    %>
                                    <img src="<%=basePath%>static/img/male1.png" alt="" width="100" height="100">
                                    <%
                                    } else {
                                    %>
                                    <img src="<%=basePath%>static/teacherimg/personphoto/<%=t.getPersonImg()%>" alt=""
                                         width="100" height="100">
                                    <%
                                        }
                                    %>
                                </dt>
                                <dd class="r_cell">
                                    <h5><%=t.getTeacherName().substring(0, 1)%>教员</h5>
                                    <p>授课经验：<%=t.getTeachingAge()%>年</p>
                                    <p><%=t.getEducationName()%>
                                    </p>
                                </dd>
                            </dl>
                        </a>
                    </li>
                    <%
                            }
                        }
                    %>

                </ul>
            </div>
            <!--end一对一家教-->
        </div>
        <!--end标签内容-->
    </div>
    <!--end一行内容-->
</div>
<!--end中小学文化课-->

<div class="h_tab w_1200 clearfix box_w_n h_course2">
    <dl class="tab_nav_w h_tab_nav">
        <dd class="fr">
            <a href="<%=basePath%>teacher/teachersPage"
               class="round_m transition_a more_a">更多+&nbsp;&nbsp;&nbsp;</a>
        </dd>
        <dt class="fl">
            <h4>艺术/乐器</h4>
        </dt>
    </dl>
    <div class="gray_border clearfix tab_box_o">
        <div class="course_class">
            <dl class="course_class_dl">
                <dt class="clearfix">
                    <span class="h_arrow png"></span>
                    <ul class="list_2 clearfix">
                        <li><a href="teacher/teacherListPage?areaId=0&teacherType=0&gender=2&course=74">钢琴</a></li>
                        <li><a href="teacher/teacherListPage?areaId=0&teacherType=0&gender=2&course=74">架子鼓</a></li>
                        <li><a href="teacher/teacherListPage?areaId=0&teacherType=0&gender=2&course=74">吉他</a></li>
                        <li><a href="teacher/teacherListPage?areaId=0&teacherType=0&gender=2&course=74">笛子</a></li>
                        <li><a href="teacher/teacherListPage?areaId=0&teacherType=0&gender=2&course=74">电子器</a></li>
                        <li><a href="teacher/teacherListPage?areaId=0&teacherType=0&gender=2&course=75">美术</a></li>
                        <li><a href="teacher/teacherListPage?areaId=0&teacherType=0&gender=2&course=75">速写</a></li>
                        <li><a href="teacher/teacherListPage?areaId=0&teacherType=0&gender=2&course=75">水粉画</a></li>
                        <li><a href="teacher/teacherListPage?areaId=0&teacherType=0&gender=2&course=75">书法</a></li>
                        <li><a href="teacher/teacherListPage?areaId=0&teacherType=0&gender=2&course=75">素描</a></li>
                        <li><a href="teacher/teacherListPage?areaId=0&teacherType=0&gender=2&course=76">街舞</a></li>
                        <li><a href="teacher/teacherListPage?areaId=0&teacherType=0&gender=2&course=76">芭蕾舞</a></li>
                        <li><a href="teacher/teacherListPage?areaId=0&teacherType=0&gender=2&course=76">瑜珈</a></li>
                        <li><a href="teacher/teacherListPage?areaId=0&teacherType=0&gender=2&course=76">拉丁舞</a></li>
                    </ul>
                </dt>
                <dd>
                    <a href="<%=basePath%>teacher/teachersPage">更多课程<span class="china">&gt;&gt;</span></a>
                </dd>
            </dl>
        </div>
        <!--end分类-->
        <div class="tab_box_w r_cell h_tab_box_w">
            <div class="hide_box" style="display: block;">
                <ul class="list_3 tearch_ul round_img">
                    <%
                        //乐器教员信息列表
                        List<Teacher> yueQiTeacherList = (List<Teacher>) request
                                .getAttribute("yueQiTeacherList");
                        if (yueQiTeacherList != null) {
                            for (int i = 0; i < yueQiTeacherList.size(); i++) {
                                Teacher t = yueQiTeacherList.get(i);
                    %>
                    <li class="li_o">
                        <a href="<%=basePath%>teacher/teacherInfoPage?tId=<%=t.getTeacherId()%>" class="box">
                            <div class="hover_div transition_a">
                                <p class="hide2">
                                    <%
                                        if (t.getSelfEvaluation().length() > 50) {
                                    %>
                                    <%=t.getSelfEvaluation().substring(0, 50)%>...
                                    <%
                                    } else {
                                    %>
                                    <%=t.getSelfEvaluation()%>
                                    <%
                                        }
                                    %>
                                </p>
                                <div class="d">
                                    <span class="look_btn transition_a">查看详情</span>
                                </div>
                            </div>
                            <dl>
                                <dt class="fl">
                                    <%
                                        if (t.getGender() == 0 && "".equals(t.getPersonImg())) {
                                    %>
                                    <img src="<%=basePath%>static/img/w.png" alt="" width="100" height="100">
                                    <%
                                    } else if (t.getGender() == 1
                                            && "".equals(t.getPersonImg())) {
                                    %>
                                    <img src="<%=basePath%>static/img/male1.png" alt="" width="100" height="100">
                                    <%
                                    } else {
                                    %>
                                    <img src="<%=basePath%>static/teacherimg/personphoto/<%=t.getPersonImg()%>" alt=""
                                         width="100" height="100">
                                    <%
                                        }
                                    %>
                                </dt>
                                <dd class="r_cell">
                                    <h5><%=t.getTeacherName().substring(0, 1)%>教员</h5>
                                    <p>授课经验：<%=t.getTeachingAge()%>年</p>
                                    <p><%=t.getEducationName()%>
                                    </p>
                                </dd>
                            </dl>
                        </a>
                    </li>
                    <%
                            }
                        }
                    %>

                </ul>
            </div>
            <!--end一对一家教-->

        </div>
        <!--end标签内容-->
    </div>
    <!--end一行内容-->
</div>
<!--end中小学兴趣课-->

<div class="h_tab w_1200 clearfix box_w_n h_course4">
    <dl class="tab_nav_w h_tab_nav">
        <dd class="fr">
            <a href="<%=basePath%>teacher/teachersPag"
               class="round_m transition_a more_a">更多+&nbsp;&nbsp;&nbsp;</a>
        </dd>
        <dt class="fl">
            <h4>棋类/体育/计算机</h4>
        </dt>
    </dl>
    <div class="gray_border clearfix tab_box_o">
        <div class="course_class">
            <dl class="course_class_dl">
                <dt class="clearfix">
                    <span class="h_arrow png"></span>
                    <ul class="list_2 clearfix">
                        <li><a href="teacher/teacherListPage?areaId=0&teacherType=0&gender=2&course=77">国际象棋</a></li>
                        <li><a href="teacher/teacherListPage?areaId=0&teacherType=0&gender=2&course=77">围棋</a></li>
                        <li><a href="teacher/teacherListPage?areaId=0&teacherType=0&gender=2&course=77">中国象棋</a></li>
                        <li><a href="teacher/teacherListPage?areaId=0&teacherType=0&gender=2&course=78">滑冰韩冰</a></li>
                        <li><a href="teacher/teacherListPage?areaId=0&teacherType=0&gender=2&course=78">空手道</a></li>
                        <li><a href="teacher/teacherListPage?areaId=0&teacherType=0&gender=2&course=78">跆拳道</a></li>
                        <li><a href="teacher/teacherListPage?areaId=0&teacherType=0&gender=2&course=78">太极</a></li>
                        <li><a href="teacher/teacherListPage?areaId=0&teacherType=0&gender=2&course=78">游泳</a></li>
                        <li><a href="teacher/teacherListPage?areaId=0&teacherType=0&gender=2&course=78">篮球</a></li>
                        <li><a href="teacher/teacherListPage?areaId=0&teacherType=0&gender=2&course=78">乒乓球</a></li>
                        <li><a href="teacher/teacherListPage?areaId=0&teacherType=0&gender=2&course=79">PS</a></li>
                        <li><a href="teacher/teacherListPage?areaId=0&teacherType=0&gender=2&course=79">网页设计</a></li>
                        <li><a href="teacher/teacherListPage?areaId=0&teacherType=0&gender=2&course=79">编程</a></li>
                    </ul>
                </dt>
                <dd>
                    <a href="<%=basePath%>teacher/teachersPage">更多课程<span class="china">&gt;&gt;</span></a>
                </dd>
            </dl>
        </div>
        <!--end分类-->
        <div class="tab_box_w r_cell h_tab_box_w">
            <div class="hide_box" style="display: block;">
                <ul class="list_3 tearch_ul round_img">
                    <%
                        //棋类教员信息列表
                        List<Teacher> qiLeiTeacherList = (List<Teacher>) request
                                .getAttribute("qiLeiTeacherList");
                        if (qiLeiTeacherList != null) {
                            for (int i = 0; i < qiLeiTeacherList.size(); i++) {
                                Teacher t = qiLeiTeacherList.get(i);
                    %>
                    <li class="li_o">
                        <a href="<%=basePath%>teacher/teacherInfoPage?tId=<%=t.getTeacherId()%>" class="box">
                            <div class="hover_div transition_a">
                                <p class="hide2">
                                    <%
                                        if (t.getSelfEvaluation().length() > 50) {
                                    %>
                                    <%=t.getSelfEvaluation().substring(0, 50)%>...
                                    <%
                                    } else {
                                    %>
                                    <%=t.getSelfEvaluation()%>
                                    <%
                                        }
                                    %>
                                </p>
                                <div class="d">
                                    <span class="look_btn transition_a">查看详情</span>
                                </div>
                            </div>
                            <dl>
                                <dt class="fl">
                                    <%
                                        if (t.getGender() == 0 && "".equals(t.getPersonImg())) {
                                    %>
                                    <img src="<%=basePath%>static/img/w.png" alt="" width="100" height="100">
                                    <%
                                    } else if (t.getGender() == 1
                                            && "".equals(t.getPersonImg())) {
                                    %>
                                    <img src="<%=basePath%>static/img/male1.png" alt="" width="100" height="100">
                                    <%
                                    } else {
                                    %>
                                    <img src="<%=basePath%>static/teacherimg/personphoto/<%=t.getPersonImg()%>" alt=""
                                         width="100" height="100">
                                    <%
                                        }
                                    %>
                                </dt>
                                <dd class="r_cell">
                                    <h5><%=t.getTeacherName().substring(0, 1)%>教员</h5>
                                    <p>授课经验：<%=t.getTeachingAge()%>年</p>
                                    <p><%=t.getEducationName()%>
                                    </p>
                                </dd>
                            </dl>
                        </a>
                    </li>

                    <%
                            }
                        }
                    %>

                </ul>
            </div>
            <!--end一对一家教-->

        </div>
        <!--end标签内容-->
    </div>
    <!--end一行内容-->
</div>
<!--end中小学棋类/体育/计算机-->



</div>

<div class="w_1200 box_w_n clearfix h_tab">
    <dl class="tab_nav_w h_tab_nav h_tab_nav2">
        <dt class="fr">
            <a href="<%=basePath%>member/ordersPage" class="round_m transition_a more_a">更多+</a>
        </dt>
        <dt class="fl">最新家教</dt>

    </dl>
    <div class="tab_box_o">
        <div class="hide_box tab_box_w">
            <div style="display: block;">
                <ul class="h_jobs list_4 clearfix hover_ul">

                    <%
                        //家教需求订单
                        List<Orders> ordersList = (List<Orders>) request
                                .getAttribute("ordersList");
                        if (ordersList != null) {
                            for (int i = 0; i < ordersList.size(); i++) {
                                Orders order = ordersList.get(i);
                    %>
                    <li>
                        <div class="box transition_a">
                            <h5 class="orange_text"><%=order.getCourseName()%>
                            </h5>
                            <div class=""><%=order.getArea()%>
                            </div>
                            <%
                                if (order.getCoursePrice() == 0) {
                            %>
                            <div style="text-align: left;margin-left: 20px; margin-top: 10px;">课酬：参照标准</div>
                            <%
                            } else {
                            %>
                            <div style="text-align: left;margin-left: 20px; margin-top: 10px;">
                                课酬：￥<%=order.getCoursePrice()%>/小时
                            </div>
                            <%
                                }
                            %>

                            <div style="text-align: left;margin-left: 20px; margin-top: 10px;">
                                身份要求：<%=order.getTeacherTypeName()%>
                            </div>

                            <%
                                if (order.getRequirements().length() < 34) {
                            %>
                            <div style="text-align: left;margin-left: 20px;margin-top: 10px;">
                                要求：<%=order.getRequirements()%>
                            </div>
                            <%
                            } else {
                            %>
                            <div style="text-align: left;margin-left: 20px;margin-top: 10px;">
                                要求：<%=order.getRequirements().substring(0, 34)%>...
                            </div>
                            <%
                                }
                            %>


                            <div class="d" style="margin-top: 10px;">
                                <a href="<%=basePath %>order/orderInfo?oId=<%=order.getoId() %>" target="_blank"
                                   class="btn_border_o transition_a">我要申请</a>
                            </div>
                        </div>
                    </li>

                    <%
                            }
                        }
                    %>
                </ul>
            </div>
            <!--end个人-->
        </div>
    </div>
    <!--end标签内容-->
</div>
<!--end招聘教员-->

<div class="w_1200 clearfix box_w_n">
    <div class="fl w_5">
        <h4 class="th_text">家教课堂</h4>
        <ul class="list_3 ke_ul clearfix">
            <li>
                <a href="<%=basePath %>paper/conHighPaperList?grade=0&course=1" target="_blank" class="transition_a">高中语文试题</a>
            </li>
            <li>
                <a href="<%=basePath %>paper/conHighPaperList?grade=0&course=2" target="_blank" class="transition_a">高中数学试题</a>
            </li>
            <li>
                <a href="<%=basePath %>paper/conHighPaperList?grade=0&course=3" target="_blank" class="transition_a">高中英语试题</a>
            </li>

            <li>
                <a href="<%=basePath %>paper/conHighPaperList?grade=0&course=4" target="_blank" class="transition_a">高中物理试题</a>
            </li>
            <li>
                <a href="<%=basePath %>paper/conHighPaperList?grade=0&course=6" target="_blank" class="transition_a">高中化学试题</a>
            </li>
            <li>
                <a href="<%=basePath %>paper/conHighPaperList?grade=0&course=5" target="_blank" class="transition_a">高中生物试题</a>
            </li>
        </ul>


    </div>


    <div class="fr w_5">
        <h4 class="th_text"></h4>
        <ul class="list_3 ke_ul clearfix">
            <li>
                <a href="<%=basePath %>paper/conHighPaperList?grade=0&course=8" target="_blank" class="transition_a">高中历史试题</a>
            </li>
            <li>
                <a href="<%=basePath %>paper/conHighPaperList?grade=0&course=9" target="_blank" class="transition_a">高中地理试题</a>
            </li>
            <li>
                <a href="<%=basePath %>paper/conHighPaperList?grade=0&course=7" target="_blank" class="transition_a">高中政治试题</a>
            </li>
            <li>
                <a href="<%=basePath %>paper/conHighPaperList?grade=0&course=10" target="_blank" class="transition_a">文综试题</a>
            </li>
            <li>
                <a href="<%=basePath %>paper/conHighPaperList?grade=0&course=11" target="_blank" class="transition_a">高中理综试题</a>
            </li>
            <li><b class="orange_link"><a href="<%=basePath %>paper/paperPage" target="_blank">更多<span
                    class="china">&gt;&gt;</span></a></b></li>
        </ul>

    </div>

</div>
<!--end一行-->
<jsp:include page="footer.jsp"></jsp:include>
<!--star_弹框-->


<!--end弹框-->


<!--star_弹框-->
<div class="cd-popup" id="AppointSuccessPop">
    <div class="cd-popup-container" style="width: 660px;">
        <a href="javascript:void(0)" class="cd-popup-close f_close_btn transition_a"></a>
        <h4 class="f_code_th">
            预约TA
            <p></p>
        </h4>

        <div class="tel_box">
            <span class="tel_arrow png round_r"></span>
            <p>预约成功！</p>
            <p>您的专属课程顾问24小时以内尽快与您联系。</p>
            <p>或您也可以致电给我们：150-6033-8989</p>
        </div>
        <!--end一行-->

    </div>
</div>


<script>
    $(".threeli").click(function () {
        var $this = $(this);
        if (!$this.hasClass("active") && $("#ulSelect li").length >= 1) {
            alert("最多可选1门课程");
            return false;
        }

        var text = $this.find("a").text();
        var id = $this.data("id");
        if (!$this.hasClass("active")) {
            $("#ulSelect").append("<li data-id=" + id + " class='active round_m'> <a data-id='" + id + "' href='javascript:void(0)'>" + text + "</a><span class='close_s'></span></li>");
            $this.addClass("active");
        } else {
            $("#ulSelect li[data-id='" + id + "']").remove();
            $this.removeClass("active");
        }
    });

    $("#ulSelect").on("click", "li", function () {
        var $this = $(this);
        $this.remove();
        $(".threeli[data-id='" + $this.data("id") + "']").removeClass("active");
    });
</script>


