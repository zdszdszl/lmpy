<%@ page language="java" import="java.util.*" pageEncoding="utf-8" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<div class="clearfix d_link min_1200">
    <div class="clearfix w_1200 d_link">
        <div class="d_link_t clearfix">
            <div class="d_link_tel fl">
                <h4>服务热线</h4>
                <p>(工作日9:00~18:00)</p>
                <div class="d orange_text big_num font_34">xxx-xxxx-xxxx</div>
            </div>
            <div class="r_cell ">
                <ul class="d_link_ul list_4 fr">
                    <li>
                        <h4>会员指南</h4>
                        <p>
                            <a href="<%=basePath %>aboutUs">关于我们</a>
                        </p>
                        <p>
                            <a href="<%=basePath %>iamteacher">我是教员</a>
                        </p>
                        <p>
                            <a href="<%=basePath %>iammember">我是学员</a>
                        </p>
                        <p>
                            <a href="<%=basePath %>disclaimer">免责声明</a>
                        </p>

                    </li>





                    <li>
                        <h4>合作</h4>
                        <p>
                            <a href="<%=basePath %>link" rel="nofollow">友情链接</a>
                        </p>
                        <p>
                            <a href="<%=basePath %>announcement" rel="nofollow">活动公告</a>
                        </p>
                    </li>
                </ul>

            </div>
        </div>
        <!--end一行-->
        <div class="d_link_city">
            <h4 class="fl">版权所有芦苇家教</h4>
        </div>
        <!--end热门城市-->
    </div>
</div>
<!--end链接-->

<!--返回顶部-->
<div class="ftool_top">
    <a href="#" class="top_ftoolab transition_a" id="ftoolTop" style="display: none;">

    </a>

</div>
<!--返回顶部-->


</body>
</html>

