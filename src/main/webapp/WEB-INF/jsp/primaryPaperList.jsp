<%@ page language="java" import="java.util.*" pageEncoding="utf-8" %>
<%@page import="com.lmpy.lmpy.core.bean.GradePrice" %>
<%@page import="com.lmpy.lmpy.core.bean.Member" %>
<%@page import="com.lmpy.lmpy.core.bean.Teacher" %>
<%@page import="com.lmpy.lmpy.core.bean.Paper" %>
<%@page import="com.lmpy.lmpy.core.bean.News" %>
<%@page import="com.lmpy.lmpy.core.bean.Notice" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="renderer" content="webkit">
    <meta name="viewport" content="width=1200">

    <title>【芦苇教育】郑州家教_免费找家教_郑州家教一对一辅导</title>

    <link rel="icon" href="<%=basePath%>static/img/1.jpg" type="image/png">


    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/css/css11.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/css/code1.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/css/base1.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/css/brief1.css">

    <link href="<%=basePath %>static/css/teach1.css" rel="stylesheet">

    <link href="<%=basePath%>static/css/ui-choose1.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="<%=basePath%>static/css/buttons1.css">

    <script type="text/javascript" src="<%=basePath%>static/js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>static/js/jquery-ui.js"></script>
    <script type="text/javascript" src="<%=basePath%>static/js/tool.js"></script>
    <link href="<%=basePath%>static/css/home1.css" rel="stylesheet">

    <script>
        var _hmt = _hmt || [];
        (function () {
            var hm = document.createElement("script");
            hm.src = "https://hm.baidu.com/hm.js?20766a1a5bc02124297ce7be22ff84b1";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();
    </script>

</head>

<body>

<div class="header min_1200">
    <div class="w_1200">
        <img class="fl" src="<%=basePath%>static/img/1.jpg" width="190" height="100">
        <h2 class="fl logo_text">
            郑州市上门家教，来芦苇家教网！
        </h2>
        <div class="header_r fr">

            <%
                Teacher teacher = (Teacher) session.getAttribute("teacher");
                Member member = (Member) session.getAttribute("member");
                if (member == null && teacher == null) {
            %>
            <span class="font_13">
                <img src="<%=basePath%>static/img/textlogo.png" width="160" height="80">

                    <b class="orange_link">
                        <a href="<%=basePath%>loginPage" class="heada"> 登录</a>
                    </b>
                    <em class="china"></em>
                    <b class="orange_link">
                        <a href="<%=basePath%>registerPage" class="heada">注册</a>
                    </b>
                    </span>

            <%
            } else {
            %>
            <span class="font_13">
                    <b class="orange_link">
                        <a href="<%=basePath%>logout" class="heada"> 退出</a>
                    </b>
                    <em class="china"></em>
                    <b class="orange_link">
                        <%
                            if (member != null) {
                        %>
	                       <a href="<%=basePath%>member/index" class="heada">我的</a>
	                   	<%
                        } else if (teacher != null) {
                        %>
	                       <a href="<%=basePath%>teacher/index" class="heada">我的</a>
	                   	<%
                            }
                        %>
                    </b>
                    </span>

            <%
                }
            %>

        </div>
        <!--end头文件右-->
    </div>
</div>
<!--end头文件-->
<div class="nav min_1200">
    <div class="tnav w_1200" id="slideNav">
        <span><a href="<%=basePath %>" class="current_nav">首页</a></span>
        <span><a href="<%=basePath %>costPage">资费标准</a></span>
        <span><a href="<%=basePath %>teacher/teachersPage">找老师</a></span>
        <span><a href="<%=basePath %>member/ordersPage">找家教</a></span>
        <span><a href="<%=basePath %>paper/paperPage" target="_blank">家教学堂</a></span>
        <span><a href="<%=basePath %>aboutUs" >必读指南</a></span>
        <i class="tavline" id="slideNavLine" style="width: 202px; left: 0px;"></i>
    </div>
</div>
<!-- end nav -->

<dl class="site_th w_1200">
    <dd>当前位置：
        <a href="<%=basePath%>">首页</a>
        <span class="china">&gt;</span>家教学堂
    </dd>
</dl>

<div id="content" style="width: 1200px;">
    <div style="width: 900px;float: left;">
        <div class="tt-h2" style=" border-bottom: 1px solid #DDDDDD;">
            <form action="conPrimaryPaperList" method="post">
                <div class="box_w_s clearfix white_bj">
                    <ul class="filter_ul clearfix">
                        <li class="clearfix">
                            <h4 class="fl" style="width: 50px;">年级：</h4>
                            <div class="r_cell r_text round_m_a click_a_o">
                                <select class="ui-choose" id="grade" name="grade">
                                    <option value="0" style="margin: 30px; ">不限</option>
                                    <option value="1" style="margin: 30px; ">一年级</option>
                                    <option value="2" style="margin: 30px; ">二年级</option>
                                    <option value="3" style="margin: 30px; ">三年级</option>
                                    <option value="4" style="margin: 30px; ">四年级</option>
                                    <option value="5" style="margin: 30px; ">五年级</option>
                                    <option value="6" style="margin: 30px; ">六年级</option>
                                </select>
                            </div>
                        </li>
                        <li class="clearfix">
                            <h4 class="fl" style="width: 50px;">学科：</h4>
                            <div class="r_cell r_text round_m_a click_a_o">
                                <select class="ui-choose" id="course" name="course">
                                    <option value="0" style="margin: 30px; ">不限</option>
                                    <option value="1" style="margin: 30px; ">语文</option>
                                    <option value="2" style="margin: 30px; ">数学</option>
                                    <option value="3" style="margin: 30px; ">英语</option>
                                </select>
                            </div>
                            <button type="submit" class="button button-action button-box"
                                    style="width: 150px;float: right;">查询
                            </button>
                        </li>

                    </ul>
                </div>
                <!--end筛选-->
            </form>
        </div>
        <h4 class="l_th w_1200 l_th_w">
            <b class="fl th_b">小学试卷</b>
        </h4>
        <div style="margin-bottom: 30px;">
            <ul class="filelist">
                <%
                    List<Paper> paperList = (List<Paper>) request.getAttribute("paperList");
                    if (paperList != null) {
                        for (int j = 0; j < paperList.size(); j++) {
                            Paper paper = paperList.get(j);
                %>
                <li class="doc">
                    <a href="<%=basePath %>paper/paperInfo?pId=<%=paper.getpId() %>">
                        <i class="ico"></i>
                        <%=paper.getTitle() %>
                    </a>
                </li>
                <%
                        }
                    }
                %>
            </ul>

        </div>
        <%=request.getAttribute("pageTool") %>
    </div>
    <div id="other" style="width: 270px;float: right;">
        <div id="problem" style="width: 270px;border: 1px solid #DDDDDD;margin-bottom: 20px;">
            <div style="height: 45px;text-align: center;border: 1px solid #DDDDDD;background-color: #3a7ec7;font-size: 22px;color: white;">
                常见问题
            </div>
            <div class="hot-list">
                <ul>
                    <%
                        List<Notice> noticeList = (List<Notice>) request.getAttribute("noticeList");
                        if (noticeList != null) {
                            for (int i = 0; i < noticeList.size(); i++) {
                                Notice notice2 = noticeList.get(i);
                    %>
                    <li>
                        <a href="<%=basePath %>notice/noticeInfo?nId=<%=notice2.getnId() %>"
                           title="<%=notice2.getTitle() %>">
			                      <span>
			                        <%=i + 1 %>、
			                      </span>
                            <%=notice2.getTitle() %>
                        </a>
                    </li>
                    <%
                            }
                        }
                    %>
                </ul>
            </div>
        </div>


        <div id="paper" style="width: 270px;border: 1px solid #DDDDDD;margin-bottom: 20px;">
            <div style="height: 45px;text-align: center;border: 1px solid #DDDDDD;background-color: #3a7ec7;font-size: 22px;color: white;">
                最新试题
            </div>

            <div class="hot-list">
                <ul>
                    <%
                        List<Paper> topTenPaperList = (List<Paper>) request.getAttribute("topTenPaperList");
                        if (topTenPaperList != null) {
                            for (int i = 0; i < topTenPaperList.size(); i++) {
                                Paper paper = topTenPaperList.get(i);
                    %>
                    <li>
                        <a href="<%=basePath %>paper/paperInfo?pId=<%=paper.getpId() %>"
                           title="<%=paper.getTitle() %>">
                            <span><%=i + 1 %>、</span>
                            <%=paper.getTitle() %>
                        </a>
                    </li>
                    <%
                            }
                        }
                    %>
                </ul>
            </div>

        </div>
        <div id="paper1" style="width: 270px;border: 1px solid #DDDDDD;margin-bottom: 20px;">

            <div style="height: 45px;text-align: center;border: 1px solid #DDDDDD;background-color: #3a7ec7;font-size: 22px;color: white;">
                最新资讯
            </div>

            <div class="hot-list">
                <ul>
                    <%
                        List<News> newsList = (List<News>) request.getAttribute("newsList");
                        if (newsList != null) {
                            for (int i = 0; i < newsList.size(); i++) {
                                News news = newsList.get(i);
                    %>
                    <li>
                        <a href="<%=basePath %>news/newsInfo?nId=<%=news.getnId() %>"
                           title="<%=news.getTitle() %>">
                            <span><%=i + 1 %>、</span>
                            <%=news.getTitle() %>
                        </a>
                    </li>
                    <%
                            }
                        }
                    %>
                </ul>
            </div>
        </div>
    </div>
</div>
<div style="clear: both;padding-bottom: 30px;"></div>
<script src="<%=basePath %>static/js/ui-choose.js"></script>
<script>
    // 将所有.ui-choose实例化
    $('.ui-choose').ui_choose();

    $(function () {
        var grade = $('#grade').data('ui-choose');
        grade.val(<%=request.getAttribute("grade")%>);
        var course = $('#course').data('ui-choose');
        course.val(<%=request.getAttribute("course")%>);
    });
</script>

<jsp:include page="footer.jsp"></jsp:include>
