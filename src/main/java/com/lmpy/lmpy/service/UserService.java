package com.lmpy.lmpy.service;

import com.lmpy.lmpy.core.bean.ResultBean;
import com.lmpy.lmpy.core.bean.UserDetails;
import com.lmpy.lmpy.core.form.UserForm;

public interface UserService {
    public ResultBean register(UserForm userForm);

    public UserDetails login(UserForm userForm);
}
