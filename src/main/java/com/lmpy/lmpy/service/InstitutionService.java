package com.lmpy.lmpy.service;

import com.lmpy.lmpy.core.bean.Institution;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface InstitutionService {


	List<Institution> findAll(int display);

	Institution findByIid(int iId);

	List<Institution> findTopThreeInstitution();

	int addViewNum(int iId);

	int stopInstitution(int iId);

	int startInstitution(int iId);

	Map<String, Object> updateInstitution(HttpServletRequest request)throws IllegalStateException, IOException;

	int updatePhoto(int iId, String photo);

	Map<String, Object> pubInstitutionInfo(HttpServletRequest request) throws IllegalStateException, IOException;

}
