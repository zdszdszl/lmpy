package com.lmpy.lmpy.service;

import com.lmpy.lmpy.core.bean.Evaluation;
import com.lmpy.lmpy.core.bean.Orders;

import java.util.Map;

public interface EvaluationService {
    Evaluation getEvaluationById(int eId);
    Map<String,Object> updateEvaluation(Orders orders, String content);
    Map<String,Object> insertEvaluation(Orders orders, String content);
}
