package com.lmpy.lmpy.service;

import com.lmpy.lmpy.core.bean.TakeOrders;

import java.util.List;
import java.util.Map;

public interface TakeOrdersService {

	public Map<String, Object> takeOrder(TakeOrders takeOrders);

	public List<TakeOrders> findTakeOrdersListByTeacherId(int teacherId);

	public List<TakeOrders> findAllTakeOrder();

	public TakeOrders findTakeOrderBytoId(int id);

	public int updateTakeOrderInfo(TakeOrders takeOrders);

	public Map<String, Object> sendOrder(TakeOrders takeOrders, String teacherName);

	public List<TakeOrders> findTakeOrderListByOId(int id);
	public Map<String,Object> agreeTeacherAppoint(int oId,int tId,String tName);

}
