package com.lmpy.lmpy.service;

import com.lmpy.lmpy.core.bean.Course;

import java.util.List;

public interface CourseService {

	public List<Course> findAllCourseList();

	public List<Course> findCourseListByIsMobileAndKemudalei(int type);

	public List<Course> findCourseBykemudalei(int kemudalei);
}
