package com.lmpy.lmpy.service;

import com.lmpy.lmpy.core.bean.Admin;

public interface AdminService {

	public Admin login(Admin admin);
	public void createAdmin(String username,String password);
}
