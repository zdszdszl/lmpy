package com.lmpy.lmpy.service;

import com.lmpy.lmpy.core.bean.DictInfo;

import java.util.List;

public interface DictInfoService {
	public List<DictInfo> findByCode(String code);
}
