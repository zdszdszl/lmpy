package com.lmpy.lmpy.service;

import com.lmpy.lmpy.core.bean.District;

import java.util.List;

public interface DistrictService {

	public List<District> findAllDistrictList();
}
