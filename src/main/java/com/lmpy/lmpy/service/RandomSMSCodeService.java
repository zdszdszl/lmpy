package com.lmpy.lmpy.service;

import com.lmpy.lmpy.core.cache.CacheKey;
import com.lmpy.lmpy.core.cache.CacheService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class RandomSMSCodeService {
    private final CacheService cacheService;
    public String SendRandomCodeTel(String Tel){
        //TODO发送随机验证码
        cacheService.set(CacheKey.RandomSMSCode_Key+Tel,"111111",60*5);
        return "111111";
    }

}
