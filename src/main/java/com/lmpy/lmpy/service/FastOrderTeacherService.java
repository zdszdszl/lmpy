package com.lmpy.lmpy.service;

import com.lmpy.lmpy.core.bean.FastOrderTeacher;

import java.util.List;
import java.util.Map;

public interface FastOrderTeacherService {

	public Map<String, Object> orderTeacher(FastOrderTeacher fastOrderTeacher);

	public List<FastOrderTeacher> findAllFastORderTeacher();

	public FastOrderTeacher findFastOrderTeacherInfoByFotId(int id);

	public int updateFastOrderTeacherInfo(FastOrderTeacher fastOrderTeacher);

	public List<FastOrderTeacher> findFastOrderTeacherInfoByTeacherId(
			int teacherId);

}
