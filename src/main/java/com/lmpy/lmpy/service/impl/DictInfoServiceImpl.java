package com.lmpy.lmpy.service.impl;

import com.lmpy.lmpy.core.bean.DictInfo;
import com.lmpy.lmpy.dao.DictInfoDao;
import com.lmpy.lmpy.service.DictInfoService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service("dictInfoService")
@Transactional
public class DictInfoServiceImpl implements DictInfoService {

	// @Autowired
	// @Qualifier("dictInfoDao")
	@Resource(name = "dictInfoDao")
	private DictInfoDao dictInfoDao;

	/*
	 * 查询数据字典数据
	 */
	public List<DictInfo> findByCode(String code) {
		List<DictInfo> dictList = dictInfoDao.findByCode(code);
		return dictList;
	}
}
