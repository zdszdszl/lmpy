package com.lmpy.lmpy.service.impl;

import com.lmpy.lmpy.core.bean.Course;
import com.lmpy.lmpy.dao.CourseDao;
import com.lmpy.lmpy.service.CourseService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional
public class CourseServiceImpl implements CourseService {

	@Resource
	private CourseDao courseDao;

	public List<Course> findAllCourseList() {

		return courseDao.findAllCourseList();
	}

	@Override
	public List<Course> findCourseListByIsMobileAndKemudalei(int type) {
		return courseDao.findCourseListByIsMobileAndKemudalei(type);
	}

	@Override
	public List<Course> findCourseBykemudalei(int kemudalei) {
		return courseDao.findCourseBykemudalei(kemudalei);
	}

}
