package com.lmpy.lmpy.service.impl;

import com.lmpy.lmpy.core.bean.Admin;
import com.lmpy.lmpy.dao.AdminDao;
import com.lmpy.lmpy.service.AdminService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
@Transactional
public class AdminServiceImpl implements AdminService {

	@Resource
	private AdminDao adminDao;

	@Override
	public Admin login(Admin admin) {

		return adminDao.getByUsernameAndPwd(admin.getUsername(), admin
				.getPassword());
	}

	@Override
	public void createAdmin(String username, String password) {
		adminDao.createAdmin(username, password);
	}

}
