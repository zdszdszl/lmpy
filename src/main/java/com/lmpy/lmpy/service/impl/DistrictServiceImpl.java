package com.lmpy.lmpy.service.impl;

import com.lmpy.lmpy.core.bean.District;
import com.lmpy.lmpy.dao.DistrictDao;
import com.lmpy.lmpy.service.DistrictService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional
public class DistrictServiceImpl implements DistrictService {

	@Resource
	private DistrictDao districtDao;

	@Override
	public List<District> findAllDistrictList() {
		return districtDao.findAllDistrictList();
	}

}
