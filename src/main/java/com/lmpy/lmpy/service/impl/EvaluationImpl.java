package com.lmpy.lmpy.service.impl;

import com.lmpy.lmpy.core.bean.Evaluation;
import com.lmpy.lmpy.core.bean.Orders;
import com.lmpy.lmpy.dao.EvaluationDao;
import com.lmpy.lmpy.service.EvaluationService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

@Service
public class EvaluationImpl implements EvaluationService {
    @Resource
    private EvaluationDao evaluationDao;
    @Override
    public Evaluation getEvaluationById(int eId) {
        return evaluationDao.getEvaluationById(eId);
    }

    @Override
    public Map<String, Object> updateEvaluation(Orders orders, String content) {
        return evaluationDao.updateEvaluation(orders,content);
    }

    @Override
    public Map<String, Object> insertEvaluation(Orders orders, String content) {
        return evaluationDao.insertEvaluation(orders,content);
    }
}
