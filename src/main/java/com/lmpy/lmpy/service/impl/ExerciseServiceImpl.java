package com.lmpy.lmpy.service.impl;

import com.lmpy.lmpy.VO.ExerciseVO;
import com.lmpy.lmpy.core.bean.ExerciseSearchRule;
import com.lmpy.lmpy.core.bean.ExerciseSet;
import com.lmpy.lmpy.core.bean.ExerciseSetRule;
import com.lmpy.lmpy.core.bean.ResultBean;
import com.lmpy.lmpy.entity.Exercise;
import com.lmpy.lmpy.entity.ExerciseAnswer;
import com.lmpy.lmpy.entity.ExerciseOption;
import com.lmpy.lmpy.mapper.*;
import com.lmpy.lmpy.mapper.ExerciseMapper;
import com.lmpy.lmpy.service.ExerciseService;
import lombok.RequiredArgsConstructor;
import org.mybatis.dynamic.sql.render.RenderingStrategy;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.lmpy.lmpy.mapper.ExerciseAnswerDynamicSqlSupport.*;
import static com.lmpy.lmpy.mapper.ExerciseDynamicSqlSupport.*;
import static com.lmpy.lmpy.mapper.ExerciseOptionDynamicSqlSupport.*;
import static org.mybatis.dynamic.sql.SqlBuilder.isEqualTo;
import static org.mybatis.dynamic.sql.select.SelectDSL.select;

/**
 * @author 董鑫明
 */
@Service
@RequiredArgsConstructor
public class ExerciseServiceImpl implements ExerciseService {
    private final ExerciseMapper exerciseMapper;
    private final ExerciseOptionMapper exerciseOptionMapper;
    private final ExerciseAnswerMapper exerciseAnswerMapper;

    /**
     *
     * @param exerciseSearchRule 搜索习题的规则
     * @return ResultBean 结果Bean的数据为搜索到的习题List
     */
    @Override
    public ResultBean SearchAll(ExerciseSearchRule exerciseSearchRule) {
        SelectStatementProvider selectStatementProvider=select(ExerciseDynamicSqlSupport.id,name,content)
                .from(exercise)
                .where(kind,isEqualTo(exerciseSearchRule.getKind()))
                .and(rank,isEqualTo(exerciseSearchRule.getRank()))
                .and(subject,isEqualTo(exerciseSearchRule.getSubject()))
                .and(difficult,isEqualTo(exerciseSearchRule.getDifficult()))
                .build()
                .render(RenderingStrategy.MYBATIS3);
        List<Exercise> exerciseList=exerciseMapper.selectMany(selectStatementProvider);
        ArrayList<ExerciseVO> exerciseVOS=new ArrayList<>();
        for (Exercise e:exerciseList
        ) {
            ExerciseVO exerciseVO=new ExerciseVO();
            exerciseVO.setId(e.getId());
            exerciseVO.setName(e.getName());
            exerciseVO.setContent(e.getContent());
            SelectStatementProvider optionsSelect=select(option,optionContent)
                    .from(exerciseOption)
                    .where(ExerciseOptionDynamicSqlSupport.exerciseId,isEqualTo(e.getId()))
                    .build()
                    .render(RenderingStrategy.MYBATIS3);
            List<ExerciseOption> exerciseOptions=exerciseOptionMapper.selectMany(optionsSelect);
            exerciseVO.getExerciseOptions().addAll(exerciseOptions);
            SelectStatementProvider answerSelect=select(answer)
                    .from(exerciseAnswer)
                    .where(ExerciseAnswerDynamicSqlSupport.exerciseId,isEqualTo(e.getId()))
                    .build()
                    .render(RenderingStrategy.MYBATIS3);
            List<ExerciseAnswer> exerciseAnswers=exerciseAnswerMapper.selectMany(answerSelect);
            exerciseVO.getExerciseAnswers().addAll(exerciseAnswers);
            exerciseVOS.add(exerciseVO);
        }
        return ResultBean.ok(exerciseVOS);
    }

    /**
     *
     * @param exerciseSetRule 创建目标习题集的规则
     * @return 创建的习题集
     */
    @Override
    public ResultBean BuildExerciseSetRandom(ExerciseSetRule exerciseSetRule) {
        ExerciseSet exerciseSet=new ExerciseSet();
        int exerciseNum=exerciseSetRule.kind.get(1)
                +exerciseSetRule.kind.get(2)
                +exerciseSetRule.kind.get(3)
                +exerciseSetRule.kind.get(4);
        ArrayList<ExerciseVO> radioExe=new ArrayList<>();//选择题
        for(int i=0;i<3;++i){
            int d=_getExerciseNumByDiff(i+1,exerciseSetRule.difficult.get(i+1));
            ArrayList<ExerciseVO> exerciseVOS=_getExerciseByRule(new ExerciseSearchRule(1,exerciseSetRule.rank,exerciseSetRule.subject,i+1));
            HashMap<Integer,ExerciseVO> resVos=new HashMap<>();
            while(resVos.size()<d){
                int pos= (int) (Math.random()*exerciseVOS.size());
                resVos.put(pos,exerciseVOS.get(pos));
            }
            radioExe.addAll(resVos.values());
        }
        ArrayList<ExerciseVO> multipleExe=new ArrayList<>();
        return ResultBean.ok(exerciseSet);
    }

    /**
     *
     * @param kind 题目种类
     * @param difficult 题目难度
     * @return 该种难度该种题的数目
     */
    private int _getExerciseNumByDiff(int kind,float difficult){
        //TODO
        return 10;
    }
    private ArrayList<ExerciseVO> _getExerciseByRule(ExerciseSearchRule exerciseSearchRule){
        ArrayList<ExerciseVO> res= (ArrayList<ExerciseVO>) SearchAll(exerciseSearchRule).getData();
        return res;
    }

}
