package com.lmpy.lmpy.service.impl;

import com.lmpy.lmpy.core.bean.GradePrice;
import com.lmpy.lmpy.dao.GradePriceDao;
import com.lmpy.lmpy.service.GradePriceService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("gradePriceService")
public class GradePriceServiceImpl implements GradePriceService {

	@Resource
	private GradePriceDao gradePriceDao;

	@Override
	public List<GradePrice> findAllCoursePriceList() {

		return gradePriceDao.findAllCoursePriceList();
	}

}
