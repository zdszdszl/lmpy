package com.lmpy.lmpy.service.impl;

import com.lmpy.lmpy.core.bean.ResultBean;
import com.lmpy.lmpy.core.bean.UserDetails;
import com.lmpy.lmpy.core.enums.ResponseStatus;
import com.lmpy.lmpy.core.exception.BusinessException;
import com.lmpy.lmpy.core.form.UserForm;
import com.lmpy.lmpy.core.utils.IdWorker;
import com.lmpy.lmpy.entity.User;
import com.lmpy.lmpy.mapper.UserMapper;
import com.lmpy.lmpy.service.UserService;
import lombok.RequiredArgsConstructor;
import org.mybatis.dynamic.sql.render.RenderingStrategies;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.lmpy.lmpy.mapper.UserDynamicSqlSupport.*;
import static org.mybatis.dynamic.sql.SqlBuilder.count;
import static org.mybatis.dynamic.sql.SqlBuilder.isEqualTo;
import static org.mybatis.dynamic.sql.select.SelectDSL.select;

/**
 * author 董鑫明，黄佳乐
 */
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserMapper userMapper;
    private final IdWorker idWorker=new IdWorker();

    /**
     *
     * @param userForm
     * @return
     * @author 董鑫明
     */
    /*
    查询用户名是否已注册
     */
    @Override
    public ResultBean register(UserForm userForm) {
        if (hasUser_username(userForm)) {
            return ResultBean.fail(ResponseStatus.USERNAME_EXIST);
        }
        User user=new User();
        user.setId(idWorker.nextId());
        user.setUsername(userForm.getUsername());
        user.setAddress(userForm.getAddress());
        user.setPassword(userForm.getPassword());
        user.setName(userForm.getName());
        user.setSex(userForm.getSex());
        user.setEmail(userForm.getEmail());
        user.setTel(userForm.getUsername());
        if(userMapper.insertSelective(user)<=0){
            return ResultBean.error();
        }
        UserDetails userDetails = new UserDetails();
        userDetails.setId(user.getId());
        userDetails.setUsername(user.getUsername());
        return ResultBean.ok(userDetails);
    }
    private boolean hasUser_username(UserForm userForm){
        SelectStatementProvider selectStatementProvider=select(count(username))
                .from(user)
                .where(username,isEqualTo(userForm.getUsername()))
                .build()
                .render(RenderingStrategies.MYBATIS3);
        long count=userMapper.count(selectStatementProvider);
        return count>0;
    }

    /*
    查询用户名是否已登录
     */
    @Override
    public UserDetails login(UserForm form) {
        //根据用户名密码查询记录
        SelectStatementProvider selectStatement = select(id, username)
                .from(user)
                .where(username, isEqualTo(form.getUsername()))
                .and(password, isEqualTo(form.getPassword()))
                .build()
                .render(RenderingStrategies.MYBATIS3);
        List<User> users = userMapper.selectMany(selectStatement);
        if (users.size() == 0) {
            throw new BusinessException(ResponseStatus.USERNAME_PASS_ERROR);
        }
        //生成UserDetail对象并返回
        UserDetails userDetails = new UserDetails();
        User user = users.get(0);
        userDetails.setId(user.getId());
        userDetails.setUsername(form.getUsername());
        return userDetails;
    }



}
