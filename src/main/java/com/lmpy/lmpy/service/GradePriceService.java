package com.lmpy.lmpy.service;

import com.lmpy.lmpy.core.bean.GradePrice;

import java.util.List;

public interface GradePriceService {
	public List<GradePrice> findAllCoursePriceList();
}
