package com.lmpy.lmpy.service;

import com.lmpy.lmpy.core.bean.ExerciseSearchRule;
import com.lmpy.lmpy.core.bean.ExerciseSetRule;
import com.lmpy.lmpy.core.bean.ResultBean;

public interface ExerciseService {
    public ResultBean SearchAll(ExerciseSearchRule exerciseSearchRule);

    public ResultBean BuildExerciseSetRandom(ExerciseSetRule exerciseSetRule);
}
