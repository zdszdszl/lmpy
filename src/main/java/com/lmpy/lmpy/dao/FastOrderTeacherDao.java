package com.lmpy.lmpy.dao;

import com.lmpy.lmpy.core.bean.FastOrderTeacher;

import java.util.List;

public interface FastOrderTeacherDao {

	public int insert(FastOrderTeacher fastOrderTeacher);

	public int calcOrderTeacherNumByIp(String ip);

	public List<FastOrderTeacher> findAllFastORderTeacher();

	public FastOrderTeacher findFastOrderTeacherInfoByFotId(int id);

	public int updateFastOrderTeacherInfo(FastOrderTeacher fastOrderTeacher);

	public List<FastOrderTeacher> findFastOrderTeacherInfoByTeacherId(
			int teacherId);
}
