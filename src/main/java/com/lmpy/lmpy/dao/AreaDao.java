package com.lmpy.lmpy.dao;

import com.lmpy.lmpy.core.bean.Area;

public interface AreaDao {

	public Area findById(int id);
	
}
