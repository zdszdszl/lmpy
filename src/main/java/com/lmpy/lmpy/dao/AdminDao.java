package com.lmpy.lmpy.dao;

import com.lmpy.lmpy.core.bean.Admin;

public interface AdminDao {

	public Admin getByUsernameAndPwd(String username, String password);
	public void createAdmin(String username, String password);
}
