package com.lmpy.lmpy.dao;

import com.lmpy.lmpy.core.bean.Institution;

import java.util.List;

public interface InstitutionDao {

	public List<Institution> findAll(int display);

	public Institution findByIid(int iId);

	public List<Institution> findTopThreeInstitution();

	public int addViewNum(int iId);

	public int updateInstitutionDisPlay(int iId, int display);

	public int updateInstitution(Institution institution);

	public int updatePhoto(int iId, String photo);

	public int insert(Institution institution);

}
