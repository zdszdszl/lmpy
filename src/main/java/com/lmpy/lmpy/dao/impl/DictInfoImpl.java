package com.lmpy.lmpy.dao.impl;

import com.lmpy.lmpy.core.bean.DictInfo;
import com.lmpy.lmpy.dao.DictInfoDao;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

@Repository("dictInfoDao")
public class DictInfoImpl implements DictInfoDao {

	@Resource(name = "jdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	public List<DictInfo> findByCode(String code) {
		
		String sql="SELECT dictid dictId,`code`,content,linkDict  FROM t_dictinfo WHERE `code` =?";

		RowMapper<DictInfo> rowMapper = new BeanPropertyRowMapper<DictInfo>(DictInfo.class);
		
		List<DictInfo> dictList= jdbcTemplate.query(sql, rowMapper,code);
		
		return dictList;
	}

}
