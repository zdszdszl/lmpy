package com.lmpy.lmpy.dao.impl;

import com.lmpy.lmpy.core.bean.Evaluation;
import com.lmpy.lmpy.core.bean.Orders;
import com.lmpy.lmpy.dao.EvaluationDao;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Repository("evaluationDao")
public class EvaluationDaoImpl implements EvaluationDao {
    @Resource
    private JdbcTemplate jdbcTemplate;
    @Override
    public Evaluation getEvaluationById(int eId) {
        try {
            String sql = "select * from t_evaluation where oId=? ";

            RowMapper<Evaluation> rowMapper = new BeanPropertyRowMapper<Evaluation>(
                    Evaluation.class);
            Evaluation evaluation = jdbcTemplate.queryForObject(sql, rowMapper, eId);

            return evaluation;

        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public Map<String, Object> insertEvaluation(Orders orders, String content) {

        try {
            String sql = "insert into t_evaluation(teacherId,memberId,oId,toId,score,evaluationContent,evaluationTime)" +
                    "values (?,?,?,?,?,?,now())";

            RowMapper<Evaluation> rowMapper = new BeanPropertyRowMapper<Evaluation>(
                    Evaluation.class);
            int row = jdbcTemplate.update(sql, orders.getTeacherId(),orders.getPublisher1(),orders.getoId(),10,10,content);
            sql="update t_orders set evaluation=? where oId=?";
            row=row&jdbcTemplate.update(sql,1,orders.getoId());
            if (row!=0)
                return (Map<String, Object>) new HashMap<String,Object>().put("success",111);
            return (Map<String, Object>) new HashMap<String,Object>().put("fail",111);

        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public Map<String, Object> updateEvaluation(Orders orders, String content) {
        try {
            String sql = "update t_evaluation set evaluationContent=? where oId=?";

            int row = jdbcTemplate.update(sql, content,orders.getoId());
            sql="update t_orders set evaluation=? where oId=?";
            if (row!=0)
                return (Map<String, Object>) new HashMap<String,Object>().put("success",111);
            return (Map<String, Object>) new HashMap<String,Object>().put("fail",111);

        } catch (Exception e) {
            return null;
        }
    }
}
