package com.lmpy.lmpy.dao.impl;

import com.lmpy.lmpy.core.bean.Admin;
import com.lmpy.lmpy.dao.AdminDao;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

@Repository("adminDao")
public class AdminDaoImpl implements AdminDao {

	@Resource
	private JdbcTemplate jdbcTemplate;

	@Override
	public Admin getByUsernameAndPwd(String username, String password) {
		try {
			String sql = "select aId,username,password from t_admin where username=? AND password=?";

			RowMapper<Admin> rowMapper = new BeanPropertyRowMapper<Admin>(
					Admin.class);
			Admin admin = jdbcTemplate.queryForObject(sql, rowMapper, username,
					password);

			return admin;
			
		} catch (Exception e) {
			return null;
		}

	}

	@Override
	public void createAdmin(String username, String password) {

	}

}
