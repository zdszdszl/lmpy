package com.lmpy.lmpy.dao;

import com.lmpy.lmpy.core.bean.District;

import java.util.List;

public interface DistrictDao {

	public List<District> findAllDistrictList();
}
