package com.lmpy.lmpy.dao;

import com.lmpy.lmpy.core.bean.Course;

import java.util.List;

public interface CourseDao {

	public List<Course> findAllCourseList();

	public Course findCourseById(int id);

	public List<Course> findCourseListByIsMobileAndKemudalei(int type);

	public List<Course> findCourseBykemudalei(int kemudalei);
}
