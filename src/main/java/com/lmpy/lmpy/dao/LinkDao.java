package com.lmpy.lmpy.dao;

import com.lmpy.lmpy.core.bean.Link;

import java.util.List;

public interface LinkDao {

	public int insert(Link link);

	public List<Link> find(int display);

	public int updateDisplay(int id, int i);

	public Link findByLid(int id);

	public int update(Link link);

}
