package com.lmpy.lmpy.dao;

import com.lmpy.lmpy.core.bean.DictInfo;

import java.util.List;

public interface DictInfoDao {

	/**
	 * 根据code查询
	 * 
	 * @param code
	 * @return List<DictInfo>
	 */
	public List<DictInfo> findByCode(String code);
	
	
}
