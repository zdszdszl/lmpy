package com.lmpy.lmpy.dao;

import com.lmpy.lmpy.core.bean.GradePrice;

import java.util.List;


public interface GradePriceDao {

	public List<GradePrice> findAllCoursePriceList();

}
