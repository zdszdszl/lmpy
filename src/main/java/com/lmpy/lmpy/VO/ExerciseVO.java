package com.lmpy.lmpy.VO;

import com.lmpy.lmpy.entity.Exercise;
import com.lmpy.lmpy.entity.ExerciseAnswer;
import com.lmpy.lmpy.entity.ExerciseOption;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;

@EqualsAndHashCode(callSuper = true)
@Data
public class ExerciseVO extends Exercise {
    private ArrayList<ExerciseOption> exerciseOptions;
    private ArrayList<ExerciseAnswer> exerciseAnswers;
    public ExerciseVO(){
        exerciseAnswers=new ArrayList<>();
        exerciseOptions=new ArrayList<>();
    }
}
