package com.lmpy.lmpy.controller;

import com.lmpy.lmpy.core.bean.ResultBean;
import com.lmpy.lmpy.core.bean.UserDetails;
import com.lmpy.lmpy.core.cache.CacheKey;
import com.lmpy.lmpy.core.cache.CacheService;
import com.lmpy.lmpy.core.enums.ResponseStatus;
import com.lmpy.lmpy.core.form.UserForm;
import com.lmpy.lmpy.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

/**
 * author 董鑫明，黄佳乐
 */
@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping(path={"/_User"})
public class UserController extends BaseController{
    private final CacheService cacheService;
    private final UserService userService;
    /**
     * 家长注册
     */
    @PostMapping(path={"register"})
    public ResultBean register( @Valid UserForm form, BindingResult result){
        //判断参数是否合法
        if (result.hasErrors()) {
            log.info(result.getAllErrors().toString());
            return ResultBean.fail(ResponseStatus.PARAM_ERROR);
        }
        if(!form.getVarcode().equals(cacheService.get(CacheKey.RandomCode_Key))){
            return ResultBean.fail(ResponseStatus.VEL_CODE_ERROR);
        }
        ResultBean res=userService.register(form);
        if(res.getCode()==ResponseStatus.OK.getCode()){
            UserDetails userDetails = (UserDetails) res.getData();
            Map<String, Object> data = new HashMap<>(1);
            data.put("token", jwtTokenUtil.generateToken(userDetails));
            return ResultBean.ok(data);
        }
        return res;
    }
    /**
     * 登陆
     */
    @PostMapping("login")
    public ResultBean login(@Valid UserForm user, BindingResult result) {
        //判断参数是否合法
        if (result.hasErrors()) {
            log.info(result.getAllErrors().toString());
            return ResultBean.fail(ResponseStatus.PARAM_ERROR);
        }
        //判断验证码是否正确
        if(!user.getVarcode().equals(cacheService.get(CacheKey.RandomCode_Key))){
            return ResultBean.fail(ResponseStatus.VEL_CODE_ERROR);
        }
        //登陆
        UserDetails userDetails = userService.login(user);
        Map<String, Object> data = new HashMap<>(1);
        data.put("token", jwtTokenUtil.generateToken(userDetails));
        return ResultBean.ok(data);
    }
    /**
     * 刷新token
     */
    @PostMapping("refreshToken")
    public ResultBean refreshToken(HttpServletRequest request) {
        String token = getToken(request);
        if (jwtTokenUtil.canRefresh(token)) {
            token = jwtTokenUtil.refreshToken(token);
            Map<String, Object> data = new HashMap<>(2);
            data.put("token", token);
            UserDetails userDetail = jwtTokenUtil.getUserDetailsFromToken(token);
            data.put("username", userDetail.getUsername());
            return ResultBean.ok(data);

        } else {
            return ResultBean.fail(ResponseStatus.NO_LOGIN);
        }
    }

}
