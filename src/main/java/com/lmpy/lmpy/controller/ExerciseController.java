package com.lmpy.lmpy.controller;

import com.lmpy.lmpy.service.ExerciseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping(path ={"/_Exercise"})
public class ExerciseController {
    private final ExerciseService exerciseService;
}
