package com.lmpy.lmpy.controller;

import com.lmpy.lmpy.core.bean.ResultBean;
import com.lmpy.lmpy.core.cache.CacheService;
import com.lmpy.lmpy.core.utils.RandomValidateCodeUtil;
import com.lmpy.lmpy.service.RandomSMSCodeService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

/**
 * @author 董鑫明
 */
@RequiredArgsConstructor
@RestController
public class RandomCodeController {
    private final RandomSMSCodeService randomSMSCodeService;
    private final RandomValidateCodeUtil randomValidateCodeUtil=new RandomValidateCodeUtil();
    private final CacheService cacheService;
    @PostMapping(path={"sendRandomCode"})
    public ResultBean sendRandomCode(@RequestParam(value="Tel") String Tel){
        return ResultBean.ok(randomSMSCodeService.SendRandomCodeTel(Tel));
    }
    @GetMapping(path={"getRandomCode"})
    public void getRandomCode(HttpServletResponse httpServletResponse){
       randomValidateCodeUtil.getRandcode(cacheService,httpServletResponse);
    }
}
