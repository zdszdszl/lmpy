package com.lmpy.lmpy.controller;

import com.lmpy.lmpy.core.bean.ExerciseSearchRule;
import com.lmpy.lmpy.core.bean.ExerciseSetRule;
import com.lmpy.lmpy.core.bean.ResultBean;
import com.lmpy.lmpy.service.ExerciseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RequiredArgsConstructor
@RestController
@RequestMapping(path = "/test")
public class TestController {
    private final ExerciseService exerciseService;
    @GetMapping(path = {"/BuildExerciseSetRandom"})
    public ResultBean buildExerciseSetRandom(){
        ExerciseSetRule rule=new ExerciseSetRule();
        HashMap<Integer,Integer> kind=new HashMap<>();
        kind.put(1,10);
        return ResultBean.ok();
    }
    @GetMapping(path = {"/GetAllExerciseByRule"})
    public ResultBean getAllExerciseByRule(){
        return exerciseService.SearchAll(new ExerciseSearchRule(1,1,1,1));
    }
}
