package com.lmpy.lmpy.controller;

import com.lmpy.lmpy.core.bean.MemberOrderTeacher;
import com.lmpy.lmpy.service.MemberOrderTeacherService;
import com.lmpy.lmpy.service.MemberService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Controller
public class MemberOrderTeacherController {

	@Resource
	private MemberOrderTeacherService memberOrderTeacherService;

	@Resource
	private MemberService memberService;

	@RequestMapping("orderTeacherInfo")
	@ResponseBody
	public Map<String, Object> orderTeacherInfo(int id) {

		Map<String, Object> map = new HashMap<String, Object>();

		MemberOrderTeacher memberOrderTeacher = memberOrderTeacherService
				.findMemberOrderTeacherByMosId(id);

		if (memberOrderTeacher == null) {
			map.put("success", false);
			map.put("message", "查询失败");
		} else {
			map.put("success", true);
			map.put("message", memberOrderTeacher);
		}
		return map;
	}

	@RequestMapping("updateOrderTeacherInfo")
	@ResponseBody
	public Map<String, Object> updateOrderTeacherInfo(
			MemberOrderTeacher memberOrderTeacher) {

		Map<String, Object> map = new HashMap<String, Object>();

		int row = memberOrderTeacherService
				.updateOrderTeacherInfoByMosId(memberOrderTeacher);

		if (row != 0) {
			map.put("success", true);
			map.put("message", "修改成功");
		} else {
			map.put("success", false);
			map.put("message", "修改失败");
		}

		return map;
	}


}
