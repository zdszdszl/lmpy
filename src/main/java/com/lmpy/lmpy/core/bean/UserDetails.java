package com.lmpy.lmpy.core.bean;

import lombok.Data;

/**
 * @author 董鑫明
 */
@Data
public class UserDetails  {

    private Long id;

    private String username;
}
