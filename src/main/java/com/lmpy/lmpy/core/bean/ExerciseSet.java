package com.lmpy.lmpy.core.bean;

import com.lmpy.lmpy.VO.ExerciseVO;
import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;

@Data
public class ExerciseSet {
    /**
     * 题目类型：
     * 1 单选
     * 2 多选
     * 3 判断
     * 4 填空
     */
    public HashMap<Integer,ArrayList<ExerciseVO>> exercises;
    public ExerciseSet(){
        exercises=new HashMap<>();
    }
}
