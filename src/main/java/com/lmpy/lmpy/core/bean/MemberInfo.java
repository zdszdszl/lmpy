package com.lmpy.lmpy.core.bean;

public class MemberInfo {
    int FinishedOrderNum;

    public int getFinishedOrderNum() {
        return FinishedOrderNum;
    }

    public void setFinishedOrderNum(int finishedOrderNum) {
        FinishedOrderNum = finishedOrderNum;
    }

    public int getFinishedAppointedTeacherNum() {
        return FinishedAppointedTeacherNum;
    }

    public void setFinishedAppointedTeacherNum(int finishedAppointedTeacherNum) {
        FinishedAppointedTeacherNum = finishedAppointedTeacherNum;
    }

    public int getOpeningOrderNum() {
        return OpeningOrderNum;
    }

    public void setOpeningOrderNum(int openingOrderNum) {
        OpeningOrderNum = openingOrderNum;
    }

    int FinishedAppointedTeacherNum;
    int OpeningOrderNum;
}
