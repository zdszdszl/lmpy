package com.lmpy.lmpy.core.bean;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class ExerciseSetRule {
    /**
     * 每种题型的题目数量
     * 题目类型：
     * 1 单选
     * 2 多选
     * 3 判断
     * 4 填空
     */
    public Map<Integer,Integer> kind;
    /**
     * 题目所属科目
     * 1 语文
     * 2 数学
     * 3 英语
     * 4 化学
     * 5 物理
     * 6 生物
     * 7 政治
     * 8 历史
     * 9 地理
     */
    public int subject;
    /**
     * 题目所属年级
     * 1-3 初一~初三
     * 4-6 高一~高三
     */
    public int rank;
    /**
     * 各难度所占比重 百分比
     * 题目难度
     * 1 简单
     * 2 偏南
     * 3 难
     */
    public Map<Integer,Float> difficult;
    public ExerciseSetRule(){
        difficult=new HashMap<>();
        kind=new HashMap<>();
    }
}
