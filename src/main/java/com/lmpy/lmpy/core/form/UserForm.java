package com.lmpy.lmpy.core.form;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
public class UserForm {
    @NotBlank(message="手机号不能为空！")
    @Pattern(regexp="^1[3|4|5|6|7|8|9][0-9]{9}$",message="手机号格式不正确！")
    private String username;
    @NotBlank(message="密码不能为空！")
    private String password;
    @NotBlank(message="请确认密码！")
    private String repeatpassword;
    @NotBlank(message="请输入姓名！")
    private String name;
    @NotBlank(message="验证码不能为空!")
    private String varcode;
    @NotBlank(message = "请确认性别")
    private String sex;
    @NotBlank(message="邮箱不能为空")
    @Pattern(regexp="^\\\\w+([-+.]\\\\w+)*@\\\\w+([-.]\\\\w+)*\\\\.\\\\w+([-.]\\\\w+)*$",message="邮箱格式不正确")
    private String email;
    @NotBlank(message="请输入地址")
    private String address;
}
