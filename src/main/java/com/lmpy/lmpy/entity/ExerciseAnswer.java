package com.lmpy.lmpy.entity;

import javax.annotation.Generated;

public class ExerciseAnswer {
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6444673+08:00", comments="Source field: exercise_answer.id")
    private Long id;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6454648+08:00", comments="Source field: exercise_answer.exercise_id")
    private Long exerciseId;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6454648+08:00", comments="Source field: exercise_answer.answer")
    private String answer;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6454648+08:00", comments="Source field: exercise_answer.id")
    public Long getId() {
        return id;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6454648+08:00", comments="Source field: exercise_answer.id")
    public void setId(Long id) {
        this.id = id;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6454648+08:00", comments="Source field: exercise_answer.exercise_id")
    public Long getExerciseId() {
        return exerciseId;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6454648+08:00", comments="Source field: exercise_answer.exercise_id")
    public void setExerciseId(Long exerciseId) {
        this.exerciseId = exerciseId;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6454648+08:00", comments="Source field: exercise_answer.answer")
    public String getAnswer() {
        return answer;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6454648+08:00", comments="Source field: exercise_answer.answer")
    public void setAnswer(String answer) {
        this.answer = answer == null ? null : answer.trim();
    }
}