package com.lmpy.lmpy.entity;

import javax.annotation.Generated;

public class ExerciseOption {
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6484568+08:00", comments="Source field: exercise_option.id")
    private Long id;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6484568+08:00", comments="Source field: exercise_option.exercise_id")
    private Long exerciseId;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6484568+08:00", comments="Source field: exercise_option._option")
    private String option;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6484568+08:00", comments="Source field: exercise_option.option_content")
    private String optionContent;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6484568+08:00", comments="Source field: exercise_option.id")
    public Long getId() {
        return id;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6484568+08:00", comments="Source field: exercise_option.id")
    public void setId(Long id) {
        this.id = id;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6484568+08:00", comments="Source field: exercise_option.exercise_id")
    public Long getExerciseId() {
        return exerciseId;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6484568+08:00", comments="Source field: exercise_option.exercise_id")
    public void setExerciseId(Long exerciseId) {
        this.exerciseId = exerciseId;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6484568+08:00", comments="Source field: exercise_option._option")
    public String getOption() {
        return option;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6484568+08:00", comments="Source field: exercise_option._option")
    public void setOption(String option) {
        this.option = option == null ? null : option.trim();
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6484568+08:00", comments="Source field: exercise_option.option_content")
    public String getOptionContent() {
        return optionContent;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6484568+08:00", comments="Source field: exercise_option.option_content")
    public void setOptionContent(String optionContent) {
        this.optionContent = optionContent == null ? null : optionContent.trim();
    }
}