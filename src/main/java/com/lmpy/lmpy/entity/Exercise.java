package com.lmpy.lmpy.entity;

import javax.annotation.Generated;

public class Exercise {
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6404771+08:00", comments="Source field: exercise.id")
    private Long id;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6404771+08:00", comments="Source field: exercise.name")
    private String name;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6404771+08:00", comments="Source field: exercise.kind")
    private Integer kind;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6404771+08:00", comments="Source field: exercise._rank")
    private Integer rank;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6404771+08:00", comments="Source field: exercise.subject")
    private Integer subject;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6414743+08:00", comments="Source field: exercise.difficult")
    private Integer difficult;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6414743+08:00", comments="Source field: exercise.content")
    private String content;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6404771+08:00", comments="Source field: exercise.id")
    public Long getId() {
        return id;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6404771+08:00", comments="Source field: exercise.id")
    public void setId(Long id) {
        this.id = id;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6404771+08:00", comments="Source field: exercise.name")
    public String getName() {
        return name;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6404771+08:00", comments="Source field: exercise.name")
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6404771+08:00", comments="Source field: exercise.kind")
    public Integer getKind() {
        return kind;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6404771+08:00", comments="Source field: exercise.kind")
    public void setKind(Integer kind) {
        this.kind = kind;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6404771+08:00", comments="Source field: exercise._rank")
    public Integer getRank() {
        return rank;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6404771+08:00", comments="Source field: exercise._rank")
    public void setRank(Integer rank) {
        this.rank = rank;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6414743+08:00", comments="Source field: exercise.subject")
    public Integer getSubject() {
        return subject;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6414743+08:00", comments="Source field: exercise.subject")
    public void setSubject(Integer subject) {
        this.subject = subject;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6414743+08:00", comments="Source field: exercise.difficult")
    public Integer getDifficult() {
        return difficult;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6414743+08:00", comments="Source field: exercise.difficult")
    public void setDifficult(Integer difficult) {
        this.difficult = difficult;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6414743+08:00", comments="Source field: exercise.content")
    public String getContent() {
        return content;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6414743+08:00", comments="Source field: exercise.content")
    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }
}