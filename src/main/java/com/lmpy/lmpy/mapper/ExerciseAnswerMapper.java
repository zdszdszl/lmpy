package com.lmpy.lmpy.mapper;

import com.lmpy.lmpy.entity.ExerciseAnswer;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;
import org.mybatis.dynamic.sql.BasicColumn;
import org.mybatis.dynamic.sql.delete.DeleteDSLCompleter;
import org.mybatis.dynamic.sql.delete.render.DeleteStatementProvider;
import org.mybatis.dynamic.sql.insert.render.InsertStatementProvider;
import org.mybatis.dynamic.sql.insert.render.MultiRowInsertStatementProvider;
import org.mybatis.dynamic.sql.select.CountDSLCompleter;
import org.mybatis.dynamic.sql.select.SelectDSLCompleter;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.mybatis.dynamic.sql.update.UpdateDSL;
import org.mybatis.dynamic.sql.update.UpdateDSLCompleter;
import org.mybatis.dynamic.sql.update.UpdateModel;
import org.mybatis.dynamic.sql.update.render.UpdateStatementProvider;
import org.mybatis.dynamic.sql.util.SqlProviderAdapter;
import org.mybatis.dynamic.sql.util.mybatis3.MyBatis3Utils;

import javax.annotation.Generated;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static com.lmpy.lmpy.mapper.ExerciseAnswerDynamicSqlSupport.*;
import static org.mybatis.dynamic.sql.SqlBuilder.isEqualTo;

@Mapper
public interface ExerciseAnswerMapper {
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6474581+08:00", comments="Source Table: exercise_answer")
    BasicColumn[] selectList = BasicColumn.columnList(id, exerciseId, answer);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6464611+08:00", comments="Source Table: exercise_answer")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    long count(SelectStatementProvider selectStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6464611+08:00", comments="Source Table: exercise_answer")
    @DeleteProvider(type=SqlProviderAdapter.class, method="delete")
    int delete(DeleteStatementProvider deleteStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6464611+08:00", comments="Source Table: exercise_answer")
    @InsertProvider(type=SqlProviderAdapter.class, method="insert")
    int insert(InsertStatementProvider<ExerciseAnswer> insertStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6464611+08:00", comments="Source Table: exercise_answer")
    @InsertProvider(type=SqlProviderAdapter.class, method="insertMultiple")
    int insertMultiple(MultiRowInsertStatementProvider<ExerciseAnswer> multipleInsertStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6464611+08:00", comments="Source Table: exercise_answer")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    @ResultMap("ExerciseAnswerResult")
    Optional<ExerciseAnswer> selectOne(SelectStatementProvider selectStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6464611+08:00", comments="Source Table: exercise_answer")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    @Results(id="ExerciseAnswerResult", value = {
        @Result(column="id", property="id", jdbcType=JdbcType.BIGINT, id=true),
        @Result(column="exercise_id", property="exerciseId", jdbcType=JdbcType.BIGINT),
        @Result(column="answer", property="answer", jdbcType=JdbcType.LONGVARCHAR)
    })
    List<ExerciseAnswer> selectMany(SelectStatementProvider selectStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6464611+08:00", comments="Source Table: exercise_answer")
    @UpdateProvider(type=SqlProviderAdapter.class, method="update")
    int update(UpdateStatementProvider updateStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6464611+08:00", comments="Source Table: exercise_answer")
    default long count(CountDSLCompleter completer) {
        return MyBatis3Utils.countFrom(this::count, exerciseAnswer, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6464611+08:00", comments="Source Table: exercise_answer")
    default int delete(DeleteDSLCompleter completer) {
        return MyBatis3Utils.deleteFrom(this::delete, exerciseAnswer, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6464611+08:00", comments="Source Table: exercise_answer")
    default int deleteByPrimaryKey(Long id_) {
        return delete(c -> 
            c.where(id, isEqualTo(id_))
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6464611+08:00", comments="Source Table: exercise_answer")
    default int insert(ExerciseAnswer record) {
        return MyBatis3Utils.insert(this::insert, record, exerciseAnswer, c ->
            c.map(id).toProperty("id")
            .map(exerciseId).toProperty("exerciseId")
            .map(answer).toProperty("answer")
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6464611+08:00", comments="Source Table: exercise_answer")
    default int insertMultiple(Collection<ExerciseAnswer> records) {
        return MyBatis3Utils.insertMultiple(this::insertMultiple, records, exerciseAnswer, c ->
            c.map(id).toProperty("id")
            .map(exerciseId).toProperty("exerciseId")
            .map(answer).toProperty("answer")
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6474581+08:00", comments="Source Table: exercise_answer")
    default int insertSelective(ExerciseAnswer record) {
        return MyBatis3Utils.insert(this::insert, record, exerciseAnswer, c ->
            c.map(id).toPropertyWhenPresent("id", record::getId)
            .map(exerciseId).toPropertyWhenPresent("exerciseId", record::getExerciseId)
            .map(answer).toPropertyWhenPresent("answer", record::getAnswer)
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6474581+08:00", comments="Source Table: exercise_answer")
    default Optional<ExerciseAnswer> selectOne(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectOne(this::selectOne, selectList, exerciseAnswer, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6474581+08:00", comments="Source Table: exercise_answer")
    default List<ExerciseAnswer> select(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectList(this::selectMany, selectList, exerciseAnswer, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6474581+08:00", comments="Source Table: exercise_answer")
    default List<ExerciseAnswer> selectDistinct(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectDistinct(this::selectMany, selectList, exerciseAnswer, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6474581+08:00", comments="Source Table: exercise_answer")
    default Optional<ExerciseAnswer> selectByPrimaryKey(Long id_) {
        return selectOne(c ->
            c.where(id, isEqualTo(id_))
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6474581+08:00", comments="Source Table: exercise_answer")
    default int update(UpdateDSLCompleter completer) {
        return MyBatis3Utils.update(this::update, exerciseAnswer, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6474581+08:00", comments="Source Table: exercise_answer")
    static UpdateDSL<UpdateModel> updateAllColumns(ExerciseAnswer record, UpdateDSL<UpdateModel> dsl) {
        return dsl.set(id).equalTo(record::getId)
                .set(exerciseId).equalTo(record::getExerciseId)
                .set(answer).equalTo(record::getAnswer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6474581+08:00", comments="Source Table: exercise_answer")
    static UpdateDSL<UpdateModel> updateSelectiveColumns(ExerciseAnswer record, UpdateDSL<UpdateModel> dsl) {
        return dsl.set(id).equalToWhenPresent(record::getId)
                .set(exerciseId).equalToWhenPresent(record::getExerciseId)
                .set(answer).equalToWhenPresent(record::getAnswer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6474581+08:00", comments="Source Table: exercise_answer")
    default int updateByPrimaryKey(ExerciseAnswer record) {
        return update(c ->
            c.set(exerciseId).equalTo(record::getExerciseId)
            .set(answer).equalTo(record::getAnswer)
            .where(id, isEqualTo(record::getId))
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6474581+08:00", comments="Source Table: exercise_answer")
    default int updateByPrimaryKeySelective(ExerciseAnswer record) {
        return update(c ->
            c.set(exerciseId).equalToWhenPresent(record::getExerciseId)
            .set(answer).equalToWhenPresent(record::getAnswer)
            .where(id, isEqualTo(record::getId))
        );
    }
}