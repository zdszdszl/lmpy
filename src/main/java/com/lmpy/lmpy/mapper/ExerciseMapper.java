package com.lmpy.lmpy.mapper;

import com.lmpy.lmpy.entity.Exercise;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;
import org.mybatis.dynamic.sql.BasicColumn;
import org.mybatis.dynamic.sql.delete.DeleteDSLCompleter;
import org.mybatis.dynamic.sql.delete.render.DeleteStatementProvider;
import org.mybatis.dynamic.sql.insert.render.InsertStatementProvider;
import org.mybatis.dynamic.sql.insert.render.MultiRowInsertStatementProvider;
import org.mybatis.dynamic.sql.select.CountDSLCompleter;
import org.mybatis.dynamic.sql.select.SelectDSLCompleter;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.mybatis.dynamic.sql.update.UpdateDSL;
import org.mybatis.dynamic.sql.update.UpdateDSLCompleter;
import org.mybatis.dynamic.sql.update.UpdateModel;
import org.mybatis.dynamic.sql.update.render.UpdateStatementProvider;
import org.mybatis.dynamic.sql.util.SqlProviderAdapter;
import org.mybatis.dynamic.sql.util.mybatis3.MyBatis3Utils;

import javax.annotation.Generated;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static com.lmpy.lmpy.mapper.ExerciseDynamicSqlSupport.*;
import static org.mybatis.dynamic.sql.SqlBuilder.isEqualTo;

@Mapper
public interface ExerciseMapper {
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6434699+08:00", comments="Source Table: exercise")
    BasicColumn[] selectList = BasicColumn.columnList(id, name, kind, rank, subject, difficult, content);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6424722+08:00", comments="Source Table: exercise")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    long count(SelectStatementProvider selectStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6424722+08:00", comments="Source Table: exercise")
    @DeleteProvider(type=SqlProviderAdapter.class, method="delete")
    int delete(DeleteStatementProvider deleteStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6424722+08:00", comments="Source Table: exercise")
    @InsertProvider(type=SqlProviderAdapter.class, method="insert")
    int insert(InsertStatementProvider<Exercise> insertStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6424722+08:00", comments="Source Table: exercise")
    @InsertProvider(type=SqlProviderAdapter.class, method="insertMultiple")
    int insertMultiple(MultiRowInsertStatementProvider<Exercise> multipleInsertStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6424722+08:00", comments="Source Table: exercise")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    @ResultMap("ExerciseResult")
    Optional<Exercise> selectOne(SelectStatementProvider selectStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6424722+08:00", comments="Source Table: exercise")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    @Results(id="ExerciseResult", value = {
        @Result(column="id", property="id", jdbcType=JdbcType.BIGINT, id=true),
        @Result(column="name", property="name", jdbcType=JdbcType.VARCHAR),
        @Result(column="kind", property="kind", jdbcType=JdbcType.INTEGER),
        @Result(column="_rank", property="rank", jdbcType=JdbcType.INTEGER),
        @Result(column="subject", property="subject", jdbcType=JdbcType.INTEGER),
        @Result(column="difficult", property="difficult", jdbcType=JdbcType.INTEGER),
        @Result(column="content", property="content", jdbcType=JdbcType.LONGVARCHAR)
    })
    List<Exercise> selectMany(SelectStatementProvider selectStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6434699+08:00", comments="Source Table: exercise")
    @UpdateProvider(type=SqlProviderAdapter.class, method="update")
    int update(UpdateStatementProvider updateStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6434699+08:00", comments="Source Table: exercise")
    default long count(CountDSLCompleter completer) {
        return MyBatis3Utils.countFrom(this::count, exercise, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6434699+08:00", comments="Source Table: exercise")
    default int delete(DeleteDSLCompleter completer) {
        return MyBatis3Utils.deleteFrom(this::delete, exercise, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6434699+08:00", comments="Source Table: exercise")
    default int deleteByPrimaryKey(Long id_) {
        return delete(c -> 
            c.where(id, isEqualTo(id_))
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6434699+08:00", comments="Source Table: exercise")
    default int insert(Exercise record) {
        return MyBatis3Utils.insert(this::insert, record, exercise, c ->
            c.map(id).toProperty("id")
            .map(name).toProperty("name")
            .map(kind).toProperty("kind")
            .map(rank).toProperty("rank")
            .map(subject).toProperty("subject")
            .map(difficult).toProperty("difficult")
            .map(content).toProperty("content")
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6434699+08:00", comments="Source Table: exercise")
    default int insertMultiple(Collection<Exercise> records) {
        return MyBatis3Utils.insertMultiple(this::insertMultiple, records, exercise, c ->
            c.map(id).toProperty("id")
            .map(name).toProperty("name")
            .map(kind).toProperty("kind")
            .map(rank).toProperty("rank")
            .map(subject).toProperty("subject")
            .map(difficult).toProperty("difficult")
            .map(content).toProperty("content")
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6434699+08:00", comments="Source Table: exercise")
    default int insertSelective(Exercise record) {
        return MyBatis3Utils.insert(this::insert, record, exercise, c ->
            c.map(id).toPropertyWhenPresent("id", record::getId)
            .map(name).toPropertyWhenPresent("name", record::getName)
            .map(kind).toPropertyWhenPresent("kind", record::getKind)
            .map(rank).toPropertyWhenPresent("rank", record::getRank)
            .map(subject).toPropertyWhenPresent("subject", record::getSubject)
            .map(difficult).toPropertyWhenPresent("difficult", record::getDifficult)
            .map(content).toPropertyWhenPresent("content", record::getContent)
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6434699+08:00", comments="Source Table: exercise")
    default Optional<Exercise> selectOne(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectOne(this::selectOne, selectList, exercise, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6434699+08:00", comments="Source Table: exercise")
    default List<Exercise> select(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectList(this::selectMany, selectList, exercise, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6434699+08:00", comments="Source Table: exercise")
    default List<Exercise> selectDistinct(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectDistinct(this::selectMany, selectList, exercise, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6444673+08:00", comments="Source Table: exercise")
    default Optional<Exercise> selectByPrimaryKey(Long id_) {
        return selectOne(c ->
            c.where(id, isEqualTo(id_))
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6444673+08:00", comments="Source Table: exercise")
    default int update(UpdateDSLCompleter completer) {
        return MyBatis3Utils.update(this::update, exercise, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6444673+08:00", comments="Source Table: exercise")
    static UpdateDSL<UpdateModel> updateAllColumns(Exercise record, UpdateDSL<UpdateModel> dsl) {
        return dsl.set(id).equalTo(record::getId)
                .set(name).equalTo(record::getName)
                .set(kind).equalTo(record::getKind)
                .set(rank).equalTo(record::getRank)
                .set(subject).equalTo(record::getSubject)
                .set(difficult).equalTo(record::getDifficult)
                .set(content).equalTo(record::getContent);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6444673+08:00", comments="Source Table: exercise")
    static UpdateDSL<UpdateModel> updateSelectiveColumns(Exercise record, UpdateDSL<UpdateModel> dsl) {
        return dsl.set(id).equalToWhenPresent(record::getId)
                .set(name).equalToWhenPresent(record::getName)
                .set(kind).equalToWhenPresent(record::getKind)
                .set(rank).equalToWhenPresent(record::getRank)
                .set(subject).equalToWhenPresent(record::getSubject)
                .set(difficult).equalToWhenPresent(record::getDifficult)
                .set(content).equalToWhenPresent(record::getContent);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6444673+08:00", comments="Source Table: exercise")
    default int updateByPrimaryKey(Exercise record) {
        return update(c ->
            c.set(name).equalTo(record::getName)
            .set(kind).equalTo(record::getKind)
            .set(rank).equalTo(record::getRank)
            .set(subject).equalTo(record::getSubject)
            .set(difficult).equalTo(record::getDifficult)
            .set(content).equalTo(record::getContent)
            .where(id, isEqualTo(record::getId))
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6444673+08:00", comments="Source Table: exercise")
    default int updateByPrimaryKeySelective(Exercise record) {
        return update(c ->
            c.set(name).equalToWhenPresent(record::getName)
            .set(kind).equalToWhenPresent(record::getKind)
            .set(rank).equalToWhenPresent(record::getRank)
            .set(subject).equalToWhenPresent(record::getSubject)
            .set(difficult).equalToWhenPresent(record::getDifficult)
            .set(content).equalToWhenPresent(record::getContent)
            .where(id, isEqualTo(record::getId))
        );
    }
}