package com.lmpy.lmpy.mapper;

import org.mybatis.dynamic.sql.SqlColumn;
import org.mybatis.dynamic.sql.SqlTable;

import javax.annotation.Generated;
import java.sql.JDBCType;

public final class ExerciseAnswerDynamicSqlSupport {
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6454648+08:00", comments="Source Table: exercise_answer")
    public static final ExerciseAnswer exerciseAnswer = new ExerciseAnswer();

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6454648+08:00", comments="Source field: exercise_answer.id")
    public static final SqlColumn<Long> id = exerciseAnswer.id;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6454648+08:00", comments="Source field: exercise_answer.exercise_id")
    public static final SqlColumn<Long> exerciseId = exerciseAnswer.exerciseId;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6454648+08:00", comments="Source field: exercise_answer.answer")
    public static final SqlColumn<String> answer = exerciseAnswer.answer;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6454648+08:00", comments="Source Table: exercise_answer")
    public static final class ExerciseAnswer extends SqlTable {
        public final SqlColumn<Long> id = column("id", JDBCType.BIGINT);

        public final SqlColumn<Long> exerciseId = column("exercise_id", JDBCType.BIGINT);

        public final SqlColumn<String> answer = column("answer", JDBCType.LONGVARCHAR);

        public ExerciseAnswer() {
            super("exercise_answer");
        }
    }
}