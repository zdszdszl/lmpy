package com.lmpy.lmpy.mapper;

import org.mybatis.dynamic.sql.SqlColumn;
import org.mybatis.dynamic.sql.SqlTable;

import javax.annotation.Generated;
import java.sql.JDBCType;

public final class ExerciseDynamicSqlSupport {
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6414743+08:00", comments="Source Table: exercise")
    public static final Exercise exercise = new Exercise();

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6414743+08:00", comments="Source field: exercise.id")
    public static final SqlColumn<Long> id = exercise.id;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6414743+08:00", comments="Source field: exercise.name")
    public static final SqlColumn<String> name = exercise.name;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6414743+08:00", comments="Source field: exercise.kind")
    public static final SqlColumn<Integer> kind = exercise.kind;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6424722+08:00", comments="Source field: exercise._rank")
    public static final SqlColumn<Integer> rank = exercise.rank;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6424722+08:00", comments="Source field: exercise.subject")
    public static final SqlColumn<Integer> subject = exercise.subject;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6424722+08:00", comments="Source field: exercise.difficult")
    public static final SqlColumn<Integer> difficult = exercise.difficult;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6424722+08:00", comments="Source field: exercise.content")
    public static final SqlColumn<String> content = exercise.content;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6414743+08:00", comments="Source Table: exercise")
    public static final class Exercise extends SqlTable {
        public final SqlColumn<Long> id = column("id", JDBCType.BIGINT);

        public final SqlColumn<String> name = column("name", JDBCType.VARCHAR);

        public final SqlColumn<Integer> kind = column("kind", JDBCType.INTEGER);

        public final SqlColumn<Integer> rank = column("_rank", JDBCType.INTEGER);

        public final SqlColumn<Integer> subject = column("subject", JDBCType.INTEGER);

        public final SqlColumn<Integer> difficult = column("difficult", JDBCType.INTEGER);

        public final SqlColumn<String> content = column("content", JDBCType.LONGVARCHAR);

        public Exercise() {
            super("exercise");
        }
    }
}