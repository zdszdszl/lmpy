package com.lmpy.lmpy.mapper;

import com.lmpy.lmpy.entity.User;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;
import org.mybatis.dynamic.sql.BasicColumn;
import org.mybatis.dynamic.sql.delete.DeleteDSLCompleter;
import org.mybatis.dynamic.sql.delete.render.DeleteStatementProvider;
import org.mybatis.dynamic.sql.insert.render.InsertStatementProvider;
import org.mybatis.dynamic.sql.insert.render.MultiRowInsertStatementProvider;
import org.mybatis.dynamic.sql.select.CountDSLCompleter;
import org.mybatis.dynamic.sql.select.SelectDSLCompleter;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.mybatis.dynamic.sql.update.UpdateDSL;
import org.mybatis.dynamic.sql.update.UpdateDSLCompleter;
import org.mybatis.dynamic.sql.update.UpdateModel;
import org.mybatis.dynamic.sql.update.render.UpdateStatementProvider;
import org.mybatis.dynamic.sql.util.SqlProviderAdapter;
import org.mybatis.dynamic.sql.util.mybatis3.MyBatis3Utils;

import javax.annotation.Generated;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static com.lmpy.lmpy.mapper.UserDynamicSqlSupport.*;
import static org.mybatis.dynamic.sql.SqlBuilder.isEqualTo;

@Mapper
public interface UserMapper {
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6315021+08:00", comments="Source Table: user")
    BasicColumn[] selectList = BasicColumn.columnList(id, username, password, name, sex, idcard, tel, address, email, balance, picture, status);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6195347+08:00", comments="Source Table: user")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    long count(SelectStatementProvider selectStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6215277+08:00", comments="Source Table: user")
    @DeleteProvider(type=SqlProviderAdapter.class, method="delete")
    int delete(DeleteStatementProvider deleteStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6215277+08:00", comments="Source Table: user")
    @InsertProvider(type=SqlProviderAdapter.class, method="insert")
    int insert(InsertStatementProvider<User> insertStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6225257+08:00", comments="Source Table: user")
    @InsertProvider(type=SqlProviderAdapter.class, method="insertMultiple")
    int insertMultiple(MultiRowInsertStatementProvider<User> multipleInsertStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6235225+08:00", comments="Source Table: user")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    @ResultMap("UserResult")
    Optional<User> selectOne(SelectStatementProvider selectStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6235225+08:00", comments="Source Table: user")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    @Results(id="UserResult", value = {
        @Result(column="id", property="id", jdbcType=JdbcType.BIGINT, id=true),
        @Result(column="username", property="username", jdbcType=JdbcType.VARCHAR, id=true),
        @Result(column="password", property="password", jdbcType=JdbcType.VARCHAR),
        @Result(column="name", property="name", jdbcType=JdbcType.VARCHAR),
        @Result(column="sex", property="sex", jdbcType=JdbcType.VARCHAR),
        @Result(column="idcard", property="idcard", jdbcType=JdbcType.VARCHAR),
        @Result(column="tel", property="tel", jdbcType=JdbcType.VARCHAR),
        @Result(column="address", property="address", jdbcType=JdbcType.VARCHAR),
        @Result(column="email", property="email", jdbcType=JdbcType.VARCHAR),
        @Result(column="balance", property="balance", jdbcType=JdbcType.DECIMAL),
        @Result(column="picture", property="picture", jdbcType=JdbcType.VARCHAR),
        @Result(column="status", property="status", jdbcType=JdbcType.INTEGER)
    })
    List<User> selectMany(SelectStatementProvider selectStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6255168+08:00", comments="Source Table: user")
    @UpdateProvider(type=SqlProviderAdapter.class, method="update")
    int update(UpdateStatementProvider updateStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6265147+08:00", comments="Source Table: user")
    default long count(CountDSLCompleter completer) {
        return MyBatis3Utils.countFrom(this::count, user, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6265147+08:00", comments="Source Table: user")
    default int delete(DeleteDSLCompleter completer) {
        return MyBatis3Utils.deleteFrom(this::delete, user, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6265147+08:00", comments="Source Table: user")
    default int deleteByPrimaryKey(Long id_, String username_) {
        return delete(c -> 
            c.where(id, isEqualTo(id_))
            .and(username, isEqualTo(username_))
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6275129+08:00", comments="Source Table: user")
    default int insert(User record) {
        return MyBatis3Utils.insert(this::insert, record, user, c ->
            c.map(id).toProperty("id")
            .map(username).toProperty("username")
            .map(password).toProperty("password")
            .map(name).toProperty("name")
            .map(sex).toProperty("sex")
            .map(idcard).toProperty("idcard")
            .map(tel).toProperty("tel")
            .map(address).toProperty("address")
            .map(email).toProperty("email")
            .map(balance).toProperty("balance")
            .map(picture).toProperty("picture")
            .map(status).toProperty("status")
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6285102+08:00", comments="Source Table: user")
    default int insertMultiple(Collection<User> records) {
        return MyBatis3Utils.insertMultiple(this::insertMultiple, records, user, c ->
            c.map(id).toProperty("id")
            .map(username).toProperty("username")
            .map(password).toProperty("password")
            .map(name).toProperty("name")
            .map(sex).toProperty("sex")
            .map(idcard).toProperty("idcard")
            .map(tel).toProperty("tel")
            .map(address).toProperty("address")
            .map(email).toProperty("email")
            .map(balance).toProperty("balance")
            .map(picture).toProperty("picture")
            .map(status).toProperty("status")
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6295077+08:00", comments="Source Table: user")
    default int insertSelective(User record) {
        return MyBatis3Utils.insert(this::insert, record, user, c ->
            c.map(id).toPropertyWhenPresent("id", record::getId)
            .map(username).toPropertyWhenPresent("username", record::getUsername)
            .map(password).toPropertyWhenPresent("password", record::getPassword)
            .map(name).toPropertyWhenPresent("name", record::getName)
            .map(sex).toPropertyWhenPresent("sex", record::getSex)
            .map(idcard).toPropertyWhenPresent("idcard", record::getIdcard)
            .map(tel).toPropertyWhenPresent("tel", record::getTel)
            .map(address).toPropertyWhenPresent("address", record::getAddress)
            .map(email).toPropertyWhenPresent("email", record::getEmail)
            .map(balance).toPropertyWhenPresent("balance", record::getBalance)
            .map(picture).toPropertyWhenPresent("picture", record::getPicture)
            .map(status).toPropertyWhenPresent("status", record::getStatus)
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6325001+08:00", comments="Source Table: user")
    default Optional<User> selectOne(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectOne(this::selectOne, selectList, user, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6325001+08:00", comments="Source Table: user")
    default List<User> select(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectList(this::selectMany, selectList, user, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6334964+08:00", comments="Source Table: user")
    default List<User> selectDistinct(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectDistinct(this::selectMany, selectList, user, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6344936+08:00", comments="Source Table: user")
    default Optional<User> selectByPrimaryKey(Long id_, String username_) {
        return selectOne(c ->
            c.where(id, isEqualTo(id_))
            .and(username, isEqualTo(username_))
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6344936+08:00", comments="Source Table: user")
    default int update(UpdateDSLCompleter completer) {
        return MyBatis3Utils.update(this::update, user, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6354913+08:00", comments="Source Table: user")
    static UpdateDSL<UpdateModel> updateAllColumns(User record, UpdateDSL<UpdateModel> dsl) {
        return dsl.set(id).equalTo(record::getId)
                .set(username).equalTo(record::getUsername)
                .set(password).equalTo(record::getPassword)
                .set(name).equalTo(record::getName)
                .set(sex).equalTo(record::getSex)
                .set(idcard).equalTo(record::getIdcard)
                .set(tel).equalTo(record::getTel)
                .set(address).equalTo(record::getAddress)
                .set(email).equalTo(record::getEmail)
                .set(balance).equalTo(record::getBalance)
                .set(picture).equalTo(record::getPicture)
                .set(status).equalTo(record::getStatus);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6364883+08:00", comments="Source Table: user")
    static UpdateDSL<UpdateModel> updateSelectiveColumns(User record, UpdateDSL<UpdateModel> dsl) {
        return dsl.set(id).equalToWhenPresent(record::getId)
                .set(username).equalToWhenPresent(record::getUsername)
                .set(password).equalToWhenPresent(record::getPassword)
                .set(name).equalToWhenPresent(record::getName)
                .set(sex).equalToWhenPresent(record::getSex)
                .set(idcard).equalToWhenPresent(record::getIdcard)
                .set(tel).equalToWhenPresent(record::getTel)
                .set(address).equalToWhenPresent(record::getAddress)
                .set(email).equalToWhenPresent(record::getEmail)
                .set(balance).equalToWhenPresent(record::getBalance)
                .set(picture).equalToWhenPresent(record::getPicture)
                .set(status).equalToWhenPresent(record::getStatus);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6374853+08:00", comments="Source Table: user")
    default int updateByPrimaryKey(User record) {
        return update(c ->
            c.set(password).equalTo(record::getPassword)
            .set(name).equalTo(record::getName)
            .set(sex).equalTo(record::getSex)
            .set(idcard).equalTo(record::getIdcard)
            .set(tel).equalTo(record::getTel)
            .set(address).equalTo(record::getAddress)
            .set(email).equalTo(record::getEmail)
            .set(balance).equalTo(record::getBalance)
            .set(picture).equalTo(record::getPicture)
            .set(status).equalTo(record::getStatus)
            .where(id, isEqualTo(record::getId))
            .and(username, isEqualTo(record::getUsername))
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6384827+08:00", comments="Source Table: user")
    default int updateByPrimaryKeySelective(User record) {
        return update(c ->
            c.set(password).equalToWhenPresent(record::getPassword)
            .set(name).equalToWhenPresent(record::getName)
            .set(sex).equalToWhenPresent(record::getSex)
            .set(idcard).equalToWhenPresent(record::getIdcard)
            .set(tel).equalToWhenPresent(record::getTel)
            .set(address).equalToWhenPresent(record::getAddress)
            .set(email).equalToWhenPresent(record::getEmail)
            .set(balance).equalToWhenPresent(record::getBalance)
            .set(picture).equalToWhenPresent(record::getPicture)
            .set(status).equalToWhenPresent(record::getStatus)
            .where(id, isEqualTo(record::getId))
            .and(username, isEqualTo(record::getUsername))
        );
    }
}