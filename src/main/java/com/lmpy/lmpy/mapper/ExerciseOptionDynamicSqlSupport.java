package com.lmpy.lmpy.mapper;

import org.mybatis.dynamic.sql.SqlColumn;
import org.mybatis.dynamic.sql.SqlTable;

import javax.annotation.Generated;
import java.sql.JDBCType;

public final class ExerciseOptionDynamicSqlSupport {
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6484568+08:00", comments="Source Table: exercise_option")
    public static final ExerciseOption exerciseOption = new ExerciseOption();

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6494546+08:00", comments="Source field: exercise_option.id")
    public static final SqlColumn<Long> id = exerciseOption.id;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6494546+08:00", comments="Source field: exercise_option.exercise_id")
    public static final SqlColumn<Long> exerciseId = exerciseOption.exerciseId;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6494546+08:00", comments="Source field: exercise_option._option")
    public static final SqlColumn<String> option = exerciseOption.option;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6494546+08:00", comments="Source field: exercise_option.option_content")
    public static final SqlColumn<String> optionContent = exerciseOption.optionContent;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6494546+08:00", comments="Source Table: exercise_option")
    public static final class ExerciseOption extends SqlTable {
        public final SqlColumn<Long> id = column("id", JDBCType.BIGINT);

        public final SqlColumn<Long> exerciseId = column("exercise_id", JDBCType.BIGINT);

        public final SqlColumn<String> option = column("_option", JDBCType.VARCHAR);

        public final SqlColumn<String> optionContent = column("option_content", JDBCType.LONGVARCHAR);

        public ExerciseOption() {
            super("exercise_option");
        }
    }
}