package com.lmpy.lmpy.mapper;

import com.lmpy.lmpy.entity.ExerciseOption;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;
import org.mybatis.dynamic.sql.BasicColumn;
import org.mybatis.dynamic.sql.delete.DeleteDSLCompleter;
import org.mybatis.dynamic.sql.delete.render.DeleteStatementProvider;
import org.mybatis.dynamic.sql.insert.render.InsertStatementProvider;
import org.mybatis.dynamic.sql.insert.render.MultiRowInsertStatementProvider;
import org.mybatis.dynamic.sql.select.CountDSLCompleter;
import org.mybatis.dynamic.sql.select.SelectDSLCompleter;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.mybatis.dynamic.sql.update.UpdateDSL;
import org.mybatis.dynamic.sql.update.UpdateDSLCompleter;
import org.mybatis.dynamic.sql.update.UpdateModel;
import org.mybatis.dynamic.sql.update.render.UpdateStatementProvider;
import org.mybatis.dynamic.sql.util.SqlProviderAdapter;
import org.mybatis.dynamic.sql.util.mybatis3.MyBatis3Utils;

import javax.annotation.Generated;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static com.lmpy.lmpy.mapper.ExerciseOptionDynamicSqlSupport.*;
import static org.mybatis.dynamic.sql.SqlBuilder.isEqualTo;

@Mapper
public interface ExerciseOptionMapper {
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6504513+08:00", comments="Source Table: exercise_option")
    BasicColumn[] selectList = BasicColumn.columnList(id, exerciseId, option, optionContent);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6494546+08:00", comments="Source Table: exercise_option")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    long count(SelectStatementProvider selectStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6494546+08:00", comments="Source Table: exercise_option")
    @DeleteProvider(type=SqlProviderAdapter.class, method="delete")
    int delete(DeleteStatementProvider deleteStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6494546+08:00", comments="Source Table: exercise_option")
    @InsertProvider(type=SqlProviderAdapter.class, method="insert")
    int insert(InsertStatementProvider<ExerciseOption> insertStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6494546+08:00", comments="Source Table: exercise_option")
    @InsertProvider(type=SqlProviderAdapter.class, method="insertMultiple")
    int insertMultiple(MultiRowInsertStatementProvider<ExerciseOption> multipleInsertStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6494546+08:00", comments="Source Table: exercise_option")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    @ResultMap("ExerciseOptionResult")
    Optional<ExerciseOption> selectOne(SelectStatementProvider selectStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6504513+08:00", comments="Source Table: exercise_option")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    @Results(id="ExerciseOptionResult", value = {
        @Result(column="id", property="id", jdbcType=JdbcType.BIGINT, id=true),
        @Result(column="exercise_id", property="exerciseId", jdbcType=JdbcType.BIGINT),
        @Result(column="_option", property="option", jdbcType=JdbcType.VARCHAR),
        @Result(column="option_content", property="optionContent", jdbcType=JdbcType.LONGVARCHAR)
    })
    List<ExerciseOption> selectMany(SelectStatementProvider selectStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6504513+08:00", comments="Source Table: exercise_option")
    @UpdateProvider(type=SqlProviderAdapter.class, method="update")
    int update(UpdateStatementProvider updateStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6504513+08:00", comments="Source Table: exercise_option")
    default long count(CountDSLCompleter completer) {
        return MyBatis3Utils.countFrom(this::count, exerciseOption, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6504513+08:00", comments="Source Table: exercise_option")
    default int delete(DeleteDSLCompleter completer) {
        return MyBatis3Utils.deleteFrom(this::delete, exerciseOption, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6504513+08:00", comments="Source Table: exercise_option")
    default int deleteByPrimaryKey(Long id_) {
        return delete(c -> 
            c.where(id, isEqualTo(id_))
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6504513+08:00", comments="Source Table: exercise_option")
    default int insert(ExerciseOption record) {
        return MyBatis3Utils.insert(this::insert, record, exerciseOption, c ->
            c.map(id).toProperty("id")
            .map(exerciseId).toProperty("exerciseId")
            .map(option).toProperty("option")
            .map(optionContent).toProperty("optionContent")
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6504513+08:00", comments="Source Table: exercise_option")
    default int insertMultiple(Collection<ExerciseOption> records) {
        return MyBatis3Utils.insertMultiple(this::insertMultiple, records, exerciseOption, c ->
            c.map(id).toProperty("id")
            .map(exerciseId).toProperty("exerciseId")
            .map(option).toProperty("option")
            .map(optionContent).toProperty("optionContent")
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6504513+08:00", comments="Source Table: exercise_option")
    default int insertSelective(ExerciseOption record) {
        return MyBatis3Utils.insert(this::insert, record, exerciseOption, c ->
            c.map(id).toPropertyWhenPresent("id", record::getId)
            .map(exerciseId).toPropertyWhenPresent("exerciseId", record::getExerciseId)
            .map(option).toPropertyWhenPresent("option", record::getOption)
            .map(optionContent).toPropertyWhenPresent("optionContent", record::getOptionContent)
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6504513+08:00", comments="Source Table: exercise_option")
    default Optional<ExerciseOption> selectOne(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectOne(this::selectOne, selectList, exerciseOption, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6504513+08:00", comments="Source Table: exercise_option")
    default List<ExerciseOption> select(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectList(this::selectMany, selectList, exerciseOption, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6514478+08:00", comments="Source Table: exercise_option")
    default List<ExerciseOption> selectDistinct(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectDistinct(this::selectMany, selectList, exerciseOption, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6514478+08:00", comments="Source Table: exercise_option")
    default Optional<ExerciseOption> selectByPrimaryKey(Long id_) {
        return selectOne(c ->
            c.where(id, isEqualTo(id_))
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6514478+08:00", comments="Source Table: exercise_option")
    default int update(UpdateDSLCompleter completer) {
        return MyBatis3Utils.update(this::update, exerciseOption, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6514478+08:00", comments="Source Table: exercise_option")
    static UpdateDSL<UpdateModel> updateAllColumns(ExerciseOption record, UpdateDSL<UpdateModel> dsl) {
        return dsl.set(id).equalTo(record::getId)
                .set(exerciseId).equalTo(record::getExerciseId)
                .set(option).equalTo(record::getOption)
                .set(optionContent).equalTo(record::getOptionContent);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6514478+08:00", comments="Source Table: exercise_option")
    static UpdateDSL<UpdateModel> updateSelectiveColumns(ExerciseOption record, UpdateDSL<UpdateModel> dsl) {
        return dsl.set(id).equalToWhenPresent(record::getId)
                .set(exerciseId).equalToWhenPresent(record::getExerciseId)
                .set(option).equalToWhenPresent(record::getOption)
                .set(optionContent).equalToWhenPresent(record::getOptionContent);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6514478+08:00", comments="Source Table: exercise_option")
    default int updateByPrimaryKey(ExerciseOption record) {
        return update(c ->
            c.set(exerciseId).equalTo(record::getExerciseId)
            .set(option).equalTo(record::getOption)
            .set(optionContent).equalTo(record::getOptionContent)
            .where(id, isEqualTo(record::getId))
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-25T13:40:31.6514478+08:00", comments="Source Table: exercise_option")
    default int updateByPrimaryKeySelective(ExerciseOption record) {
        return update(c ->
            c.set(exerciseId).equalToWhenPresent(record::getExerciseId)
            .set(option).equalToWhenPresent(record::getOption)
            .set(optionContent).equalToWhenPresent(record::getOptionContent)
            .where(id, isEqualTo(record::getId))
        );
    }
}