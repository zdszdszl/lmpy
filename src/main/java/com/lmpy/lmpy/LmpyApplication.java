package com.lmpy.lmpy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@EnableCaching
@SpringBootApplication
public class LmpyApplication {

    public static void main(String[] args) {
        SpringApplication.run(LmpyApplication.class, args);
    }

}
